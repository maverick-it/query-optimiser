package com.rule.client;

import java.sql.SQLException;
import java.util.List;

/**
 * @author 388524
 *
 */
public abstract class TableDetailsClient implements RuleClient {

	public abstract List<String> fetchTableNames();

    public abstract boolean fetchTableColumnDescription(String tableName) throws SQLException;

	public abstract void fetchTableKeysDescription(String tableName)
			throws SQLException;

    public abstract void fetchTablePartitions(String tableName) throws SQLException;

    public abstract void fetchTableIndexes(String tableName) throws SQLException;

	public abstract void setQueryText(String _queryTextLocal);
}
