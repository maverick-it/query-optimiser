package com.rule.client;

import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.rule.client.impl.DefaultPartitionRuleClient;

import java.util.Map;

/**
 * The abstract class declares the PartitionRuleClient. The corresponding
 * implementation can be found at {@link DefaultPartitionRuleClient}
 */
public abstract class PartitionRuleClient implements RuleClient {

	public abstract void executePartitionRuleEngine(
			Map<String, QueryBeans> _fullTableScanLocal,
			Map<String, TableInformationBeans> _tableInformationLocal,
			String query);

	public abstract void checkIsTablePartitioned(
			Map<String, QueryBeans> fullTableScan,
			Map<String, TableInformationBeans> tableInformation, String query);

}
