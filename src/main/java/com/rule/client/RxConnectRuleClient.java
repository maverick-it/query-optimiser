package com.rule.client;

import java.util.Map;

import com.bean.QueryBeans;
import com.bean.TableInformationBeans;

/**
 * @author 388524
 *
 */
public abstract class RxConnectRuleClient implements RuleClient{

	public abstract void executeRxConnectRules(Map<String, QueryBeans> _fullTableScanLocal,
			Map<String, TableInformationBeans> _tableInformationLocal);

}
