package com.rule.client;


/**
 * The class is based on the factory pattern and provides a factory for various
 * types of RuleCLients. The rule clients are created based on the query passed
 * as input parameter
 *
 */
public class RuleFactory {

	/**
	 * The method will create an instance of RuleClient based on the query
	 * passed. The rule clients are heavy to create and initialize. These can be
	 * cached or pooled in the future releases.
	 * 
	 * @param queryText
	 * @return
	 */
	public static Rule create(String queryText) {
		return new Rule(queryText);
	}

}
