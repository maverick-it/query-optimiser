package com.rule.client;

import java.util.Map;

import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.rule.client.impl.DefaultGeneralRuleClient;

/**
 * The abstract class declares the GeneralRuleClient. The corresponding
 * implementation can be found at {@link DefaultGeneralRuleClient}
 */
public abstract class GeneralRuleClient implements RuleClient {

	public abstract void fetchQueryPlan(
			Map<String, QueryBeans> _fullTableScanLocal) throws Exception;

	public abstract void executeGeneralRuleEngine(String queryText,
			Map<String, QueryBeans> _fullTableScanLocal,
			Map<String, TableInformationBeans> _tableInformationLocal);

	public abstract void executeMergerCartesianRule(
			Map<String, QueryBeans> fullTableScan,
			Map<String, TableInformationBeans> tableInformation, String query);

}
