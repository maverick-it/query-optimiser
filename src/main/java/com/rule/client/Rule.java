package com.rule.client;


import com.rule.client.impl.DefaultDataBaseClient;
import com.rule.client.impl.DefaultGeneralRuleClient;
import com.rule.client.impl.DefaultHardParseRuleClient;
import com.rule.client.impl.DefaultIndexRuleClient;
import com.rule.client.impl.DefaultPartitionRuleClient;
import com.rule.client.impl.DefaultRxConnectRuleClient;
import com.rule.client.impl.DefaultTableDetailsClient;

/**
 * The class as returned by the corresponding factory will denote a container
 * for various other rule clients. This class will maintain has-a relationship
 * with the following Clients:
 * 
 * <ol>
 * <li>DefaultPartitionRuleClient
 * <li>DefaultIndexRuleClient
 * <li>DefaultGeneralRuleClient
 * <li>DefaultDataBaseClient
 * <li>DefaultRxConnectRuleClient
 * </ol>
 * 
 * 
 * Any new client created should be added here as well
 * 
 * @see DefaultPartitionRuleClient
 * @see DefaultIndexRuleClient
 * @see DefaultGeneralRuleClient
 * @see DefaultDataBaseClient
 * @see DefaultRxConnectRuleClient
 *
 */
public class Rule {

	private RuleClient 		partitionRuleClient 		= 			new DefaultPartitionRuleClient();
	private RuleClient 		indexRuleClient 			= 			new DefaultIndexRuleClient();
	private RuleClient 		generalRuleClient 			= 			new DefaultGeneralRuleClient();
	private RuleClient 		dataBaseClient 				= 			new DefaultDataBaseClient();
	private RuleClient 		rxConnectRuleClient			= 			new DefaultRxConnectRuleClient();

	private RuleClient 		tableDetailsClient 			= 			null;
	private RuleClient 		hardParseRuleClient 		= 			null;

	private String 			queryText 					= 			null;

	protected Rule(String queryText) {
		this.queryText = queryText;
		tableDetailsClient = new DefaultTableDetailsClient(queryText);
	}

	public RuleClient partitionRuleClient() {
		return null != partitionRuleClient ? partitionRuleClient
				: new DefaultPartitionRuleClient();
	}

	public RuleClient indexRuleClient() {
		return null != indexRuleClient ? indexRuleClient
				: new DefaultIndexRuleClient();
	}

	public RuleClient generalRuleClient() {
		return null != generalRuleClient ? generalRuleClient
				: new DefaultGeneralRuleClient();
	}

	public RuleClient databaseClient() {
		return null != dataBaseClient ? dataBaseClient
				: new DefaultDataBaseClient();
	}

	public RuleClient rxConnectRuleClient() {
		return null != rxConnectRuleClient ? rxConnectRuleClient
				: new DefaultRxConnectRuleClient();
	}

	public RuleClient tableDetailsClient() {
		return null != tableDetailsClient ? tableDetailsClient
				: new DefaultTableDetailsClient(this.queryText);
	}

	public RuleClient hardParseRuleClient() {
		return null != hardParseRuleClient ? hardParseRuleClient
				: new DefaultHardParseRuleClient();
	}

	public String getQueryText() {
		return queryText;
	}
}