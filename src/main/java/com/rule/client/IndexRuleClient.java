package com.rule.client;

import java.util.Map;

import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.rule.client.impl.DefaultIndexRuleClient;

/**
 * The abstract class declares the IndexRuleClient. The corresponding
 * implementation can be found at {@link DefaultIndexRuleClient}
 */
public abstract class IndexRuleClient implements RuleClient {

	/**
	 * This method is used to accept Query details from parser and execute Index
	 * Rules on it.
	 */
	public abstract void executeIndexRuleEngine(
			Map<String, QueryBeans> _fullTableScanLocal,
			Map<String, TableInformationBeans> _tableInformationLocal,
			String query);
}
