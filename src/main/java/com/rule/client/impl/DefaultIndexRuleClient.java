package com.rule.client.impl;

import com.bean.ColumnDetailsBean;
import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.propertiesutil.UtilityClass;
import com.rule.client.IndexRuleClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;


/**
 * This class is Impl class for Index rule engine
 * 
 * @author 388524
 * 
 */
public class DefaultIndexRuleClient extends IndexRuleClient {

	public void executeIndexRuleEngine(
            Map<String, QueryBeans> _fullTableScanLocal,
            Map<String, TableInformationBeans> _tableInformationLocal, String query) {

		Iterator<Entry<String, QueryBeans>> queryDetails = _fullTableScanLocal
		.entrySet().iterator();
		List<String> indexKeyList = null;
		String isFastFullScanKey = null;
		while (queryDetails.hasNext()) {

			Entry<String, QueryBeans> entry = queryDetails.next();
			QueryBeans queryBean = entry.getValue();

			if (null != queryBean && null != queryBean.getExplainPlan()
					&& null != queryBean.getQuery() && null != query &&  queryBean.getQuery().trim().equalsIgnoreCase(query.trim())) {

				List<Map<String, Map<String, String>>> operationsList = queryBean
				.getExplainPlan();

				if (null != operationsList) {
					for (Map<String, Map<String, String>> operation : operationsList) {

						if (operation.containsKey("INDEX RANGE SCAN")) {
							if (null == indexKeyList) {
                                indexKeyList = new ArrayList<>();
                            }
                            Map<String, String> operationDetails = operation
							.get("INDEX RANGE SCAN");
							operationDetails.get("Name");
							indexKeyList.add(operationDetails.get("Name"));
						}
						if (operation.containsKey("INDEX UNIQUE SCAN")) {
							if (null == indexKeyList) {
                                indexKeyList = new ArrayList<>();
                            }
                            Map<String, String> operationDetails = operation
							.get("INDEX UNIQUE SCAN");
							operationDetails.get("Name");
							indexKeyList.add(operationDetails.get("Name"));
						}
						if (operation.containsKey("INDEX FAST FULL SCAN")) {
							if (null == indexKeyList) {
                                indexKeyList = new ArrayList<>();
                            }
                            Map<String, String> operationDetails = operation
							.get("INDEX FAST FULL SCAN");
							operationDetails.get("Name");
							indexKeyList.add(operationDetails.get("Name"));
						}
						if (operation.containsKey("INDEX SKIP SCAN")) {
							if (null == indexKeyList) {
                                indexKeyList = new ArrayList<>();
                            }
                            Map<String, String> operationDetails = operation
							.get("INDEX SKIP SCAN");
							operationDetails.get("Name");
							indexKeyList.add(operationDetails.get("Name"));
						}
					}
				}
				if (null != indexKeyList) {
					Set<String> keySet = _tableInformationLocal.keySet();
                    for (String tableName : keySet) {
                        boolean isIndexKeyUsed = false;
                        String indexKeyUsed = null;
						TableInformationBeans tableData = _tableInformationLocal
                                .get(tableName);
                        for (String indexKey : indexKeyList) {
                            if (tableData.getTableIndexesDescription()
									.containsKey(indexKey)) {
								isIndexKeyUsed = true;
								indexKeyUsed = indexKey;
							}
						}
						if (!isIndexKeyUsed) {
							if (null != tableData.getTableIndexesDescription()
									&& !tableData.getTableIndexesDescription()
											.isEmpty()
                                    && queryBean.getQuery().contains(tableName)) {
                                if ("Y".equalsIgnoreCase(UtilityClass
                                        .getSetRules("IndexKeyNotUsedForTable"))) {
									Map<String, List<String>> indexDetails = tableData
											.getTableIndexesDescription();
									Set<String> indexSets = indexDetails
											.keySet();
									OptimizeSuggestBean violation = new OptimizeSuggestBean();
									violation
											.setMessage("msg.IndexKeyNotUsedForTable");
                                    violation.setTableName(tableName);
                                    violation.setColumnName(indexSets
                                            .toString());
									queryBean.setViolation(violation);
								}
							}

						} else {
                            List<String> keysIndexed = tableData
                                    .getTableIndexesDescription().get(
                                            indexKeyUsed);
							StringBuffer columnNotUsed = null;
							for (String key : keysIndexed) {
								boolean columnFound = false;
								
								String alias = null == queryBean.getAliasMap() ? null
										: queryBean.getAliasMap()
                                        .get(tableName);

                                String columnNameForTableInQuery;
                                if (null != alias
                                        && null != queryBean.getAliasMap()
										&& queryBean.getAliasMap().size() > 1) {
									columnNameForTableInQuery = alias + "."
									+ key;
								} else {
									columnNameForTableInQuery = key;
								}
                                for (Entry<String, QueryBeans> entryTemp : _fullTableScanLocal
                                        .entrySet()) {
                                    if (null != entryTemp) {
                                        QueryBeans queryBeanTemp = entryTemp
                                                .getValue();
                                        if (null != queryBeanTemp
                                                .getWhereClauseColumnList()) {

                                            List<ColumnDetailsBean> colBean = queryBeanTemp
                                                    .getWhereClauseColumnList();

                                            for (ColumnDetailsBean cb : colBean) {

                                                String columnName = cb
                                                        .getColumnName();
                                                String columnValue = cb
                                                        .getColumnValue();

                                                if (alias == null) {

                                                    TableInformationBeans tableInformationBeans = _tableInformationLocal
                                                            .get(tableName);
                                                    List<String> aliasList = tableInformationBeans
                                                            .getAliasName();

                                                    if (null != aliasList) {
                                                        for (String aliasTemp : aliasList) {

                                                            String columnNameForTableInQueryTemp = aliasTemp
                                                                    + "."
                                                                    + columnNameForTableInQuery;

                                                            if (columnName
                                                                    .contains(columnNameForTableInQueryTemp)
                                                                    || columnValue
                                                                    .contains(columnNameForTableInQueryTemp)) {

                                                                columnFound = true;
                                                                break;
                                                            }

                                                        }
                                                        if (columnFound) {
                                                            break;
                                                        }
                                                    } else if (columnName
                                                            .contains(columnNameForTableInQuery)
                                                            || columnValue
                                                            .contains(columnNameForTableInQuery)) {

                                                        columnFound = true;
                                                        break;
                                                    }

                                                } else if (columnName
                                                        .contains(columnNameForTableInQuery)
                                                        || columnValue
                                                        .contains(columnNameForTableInQuery)) {

                                                    columnFound = true;
                                                    break;
                                                }

                                            }

                                        }
                                    }
                                }
                                if (!columnFound
                                        && indexKeyUsed
										.equalsIgnoreCase(isFastFullScanKey)) {
                                    break;
                                }
                                if (!columnFound) {
									if (null == columnNotUsed) {
										columnNotUsed = new StringBuffer();
										columnNotUsed.append(key);
									} else {
										columnNotUsed.append(',');
										columnNotUsed.append(key);
									}
								}
							}
							if (null != columnNotUsed) {
								if ("Y".equalsIgnoreCase(UtilityClass
										.getSetRules("UseFullIndexClause"))) {
									OptimizeSuggestBean violation = new OptimizeSuggestBean();
									violation
											.setMessage("msg.useFullIndexClause");
									violation.setColumnName(columnNotUsed
											.toString());
                                    violation.setTableName(tableName);
                                    queryBean.setViolation(violation);
                                }
							}
						}
					}
				} else {
					if (null != queryBean.getAliasMap()
							&& null != queryBean.getAliasMap().keySet()) {
						Set<String> keySet = queryBean.getAliasMap().keySet();

                        for (String tableName : keySet) {
                            Map<String, Integer> indexToIncludeMap = null;
                            TableInformationBeans tableData = _tableInformationLocal
                                    .get(tableName);
                            if (null != tableData
                                    && null != tableData
											.getTableIndexesDescription()
									&& !tableData.getTableIndexesDescription()
											.isEmpty()) {
								List<ColumnDetailsBean> whereColumnList = queryBean
										.getWhereClauseColumnList();
								StringBuffer indexToUsed = null;
								if (null != whereColumnList) {
									Map<String, List<String>> indexDetails = tableData
											.getTableIndexesDescription();
									Set<String> indexSets = indexDetails
											.keySet();
									for (String indexString : indexSets) {
										List<String> indexExistsList = tableData
												.getTableIndexesDescription()
												.get(indexString);
										int indexSize = indexExistsList.size();
										int indexUsedInWhereClauseSize = 0;
										for (String indexExist : indexExistsList) {
											for (ColumnDetailsBean whereColumn : whereColumnList) {
												String tableIndexKey = whereColumn
														.getColumnName();
												if (tableIndexKey.contains(".")) {
													int index = tableIndexKey
															.indexOf('.');
													int tableIndexSize = tableIndexKey
															.length();
													tableIndexKey = tableIndexKey
															.substring(
																	(index + 1),
																	tableIndexSize);
												}

												if (indexExist
														.equalsIgnoreCase(tableIndexKey)) {
													/*
													 * if (tableData
													 * .getTableIndexesDescription
													 * () .get(indexString)
													 * .contains(tableIndexKey))
													 * {
													 */

													indexUsedInWhereClauseSize++;
													if (null == indexToUsed) {
														indexToUsed = new StringBuffer();
														indexToUsed
																.append(indexString);
													} else {
														if (!indexToUsed
																.toString()
																.contains(
																		indexString)) {
															indexToUsed
																	.append(',');
															indexToUsed
																	.append(indexString);
														}
													}
												}
											}
										}
										if (indexUsedInWhereClauseSize > 0) {
											if (null == indexToIncludeMap) {
                                                indexToIncludeMap = new HashMap<>();
                                            }
                                            indexToIncludeMap
													.put(indexString,
															(indexSize - indexUsedInWhereClauseSize));
										}
									}
								}
								if ("Y".equalsIgnoreCase(UtilityClass
										.getSetRules("IndexKeyNotUsedForTable"))) {
									if (null != indexToIncludeMap) {
										String indexToPrint = sortByValue(indexToIncludeMap);
										OptimizeSuggestBean violation = new OptimizeSuggestBean();
										violation
												.setMessage("msg.IndexKeyNotUsedForTable");
                                        violation.setTableName(tableName);
                                        violation.setColumnName(indexToPrint);
                                        // System.out.println(indexToUsed.toString());
										queryBean.setViolation(violation);
									} else {
										Map<String, List<String>> indexDetails = tableData
												.getTableIndexesDescription();
										Set<String> indexSets = indexDetails
												.keySet();
										OptimizeSuggestBean violation = new OptimizeSuggestBean();
										violation
												.setMessage("msg.IndexKeyNotUsedForTable");
                                        violation.setTableName(tableName);
                                        violation.setColumnName(indexSets
                                                .toString());
										queryBean.setViolation(violation);
									}
								}
							}
						}
					}else if(null != _tableInformationLocal && null!=_tableInformationLocal.keySet()){

						Set<String> keySet = _tableInformationLocal.keySet();

                        for (String tableName : keySet) {
                            Map<String, Integer> indexToIncludeMap = null;
                            TableInformationBeans tableData = _tableInformationLocal
                                    .get(tableName);
                            if (null != tableData
                                    && null != tableData
											.getTableIndexesDescription()
									&& !tableData.getTableIndexesDescription()
											.isEmpty()) {
								List<ColumnDetailsBean> whereColumnList = queryBean
										.getWhereClauseColumnList();
								StringBuffer indexToUsed = null;
								if (null != whereColumnList) {
									Map<String, List<String>> indexDetails = tableData
											.getTableIndexesDescription();
									Set<String> indexSets = indexDetails
											.keySet();
									for (String indexString : indexSets) {
										List<String> indexExistsList = tableData
												.getTableIndexesDescription()
												.get(indexString);
										int indexSize = indexExistsList.size();
										int indexUsedInWhereClauseSize = 0;
										for (String indexExist : indexExistsList) {
											for (ColumnDetailsBean whereColumn : whereColumnList) {
												String tableIndexKey = whereColumn
														.getColumnName();
												if (tableIndexKey.contains(".")) {
													int index = tableIndexKey
															.indexOf('.');
													int tableIndexSize = tableIndexKey
															.length();
													tableIndexKey = tableIndexKey
															.substring(
																	(index + 1),
																	tableIndexSize);
												}

												if (indexExist
														.equalsIgnoreCase(tableIndexKey)) {
													/*
													 * if (tableData
													 * .getTableIndexesDescription
													 * () .get(indexString)
													 * .contains(tableIndexKey))
													 * {
													 */

													indexUsedInWhereClauseSize++;
													if (null == indexToUsed) {
														indexToUsed = new StringBuffer();
														indexToUsed
																.append(indexString);
													} else {
														if (!indexToUsed
																.toString()
																.contains(
																		indexString)) {
															indexToUsed
																	.append(',');
															indexToUsed
																	.append(indexString);
														}
													}
												}
											}
										}
										if (indexUsedInWhereClauseSize > 0) {
											if (null == indexToIncludeMap) {
                                                indexToIncludeMap = new HashMap<>();
                                            }
                                            indexToIncludeMap
													.put(indexString,
															(indexSize - indexUsedInWhereClauseSize));
										}
									}
								}
								if ("Y".equalsIgnoreCase(UtilityClass
										.getSetRules("IndexKeyNotUsedForTable"))) {
									if (null != indexToIncludeMap) {
										String indexToPrint = sortByValue(indexToIncludeMap);
										OptimizeSuggestBean violation = new OptimizeSuggestBean();
										violation
												.setMessage("msg.IndexKeyNotUsedForTable");
                                        violation.setTableName(tableName);
                                        violation.setColumnName(indexToPrint);
                                        // System.out.println(indexToUsed.toString());
										queryBean.setViolation(violation);
									} else {
										Map<String, List<String>> indexDetails = tableData
												.getTableIndexesDescription();
										Set<String> indexSets = indexDetails
												.keySet();
										OptimizeSuggestBean violation = new OptimizeSuggestBean();
										violation
												.setMessage("msg.IndexKeyNotUsedForTable");
                                        violation.setTableName(tableName);
                                        violation.setColumnName(indexSets
                                                .toString());
										queryBean.setViolation(violation);
									}
								}
							}
						}
					
					}
				}
			}
		}
	}


    String sortByValue(
            Map<String, Integer> map) {
        List<Entry<String, Integer>> list = new LinkedList<>(
                map.entrySet());
        Collections.sort(list, new Comparator<Entry<String, Integer>>() {
            public int compare(Entry<String, Integer> o1,
                               Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
		});

		StringBuffer result = null;
		Integer val = null;
		for (Entry<String, Integer> entry : list) {
			if (null == val) {
				val = entry.getValue();
			}
            if (Objects.equals(val, entry.getValue())) {
                if (null == result) {
                    result = new StringBuffer();
					result.append(entry.getKey());
				} else {
					result.append(',');
					result.append(entry.getKey());
				}
			}
		}
        assert result != null;
        return result.toString();
    }

}
