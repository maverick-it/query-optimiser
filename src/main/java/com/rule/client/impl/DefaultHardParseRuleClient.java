package com.rule.client.impl;


import com.propertiesutil.UtilityClass;
import com.rule.client.HardParseRuleClient;

import java.util.HashMap;
import java.util.Map;

public class DefaultHardParseRuleClient extends HardParseRuleClient {
    @Override
    public Map<String, String> executeHardParseRule(String fetchPlan) {
        Map<String, String> result = new HashMap<>();
        if ("Hard Parsing".equalsIgnoreCase(fetchPlan)) {
            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("HardParsing"))) {
                result.put("isHardParsing", "true");
            }
            result.put("fetchPlan", "Query Plan of the statement can not be viewed this time because query is invalid. Kindly re-write the statement");
        }
        return result;
    }
}
