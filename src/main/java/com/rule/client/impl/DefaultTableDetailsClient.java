package com.rule.client.impl;

import com.optimiser.SQLQueryOptimizer;
import com.parser.util.SQLParserUtil;
import com.rule.client.DataBaseClient;
import com.rule.client.Rule;
import com.rule.client.RuleClient;
import com.rule.client.RuleFactory;
import com.rule.client.TableDetailsClient;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultTableDetailsClient extends TableDetailsClient {

	private String queryText;

	public DefaultTableDetailsClient(String _queryTextLocal) {

		this.queryText = _queryTextLocal;
	}

	public List<String> fetchTableNames() {

		List<String> tableList;

		tableList = SQLParserUtil.getTableNames(queryText);

		return tableList;
	}

	/**
	 * This method is used to fetch the information about table columns.
	 * 
	 */
    public boolean fetchTableColumnDescription(String tableName) throws SQLException {

		boolean isTableExists = false;
		ResultSet resultSet = null;
		RuleClient databaseClient = null;
		Connection con = null;
		try {
			Map<String, List<Object>> tableColumnDescriptionTemp = new HashMap<>();
			if (null == SQLQueryOptimizer.getTableColumnDescription()) {
				SQLQueryOptimizer.setTableColumnDescription(new HashMap<>());
			}
			String queryToExecute = "Select * from " + tableName + "";

			Rule rule = RuleFactory.create(queryToExecute);

			databaseClient = rule.databaseClient();
			con = ((DataBaseClient) databaseClient).getConnection();
			resultSet = ((DataBaseClient) databaseClient).executeQuery(
					queryToExecute, con);

            if (null != resultSet.getMetaData()) {
                isTableExists = true;
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

				int numberOfColumns = resultSetMetaData.getColumnCount();

				for (int i = 1; i <= numberOfColumns; i++) {
					List<Object> rowData = new ArrayList<>();
					rowData.add(resultSetMetaData.getColumnName(i));
					rowData.add(resultSetMetaData.getColumnType(i));
					rowData.add(resultSetMetaData.getColumnDisplaySize(i));
					rowData.add(resultSetMetaData.getColumnTypeName(i));
					rowData.add(resultSetMetaData.isNullable(i));
					rowData.add(resultSetMetaData.isAutoIncrement(i));
					tableColumnDescriptionTemp.put(
							resultSetMetaData.getColumnName(i), rowData);
				}

				SQLQueryOptimizer.getTableBeans().setTableColumnDescription(
						tableColumnDescriptionTemp);
				SQLQueryOptimizer.getTableColumnDescription().put(tableName,
						tableColumnDescriptionTemp);
			}
		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		} finally {

			if (null != databaseClient) {
                assert resultSet != null;
                ((DataBaseClient) databaseClient).clearResource(resultSet.getStatement(),
                        resultSet, con);
            }
        }
        return isTableExists;
	}

	/**
	 * This method i used to fetch the information about the keys of the table
	 * 
	 * @throws java.sql.SQLException
	 */

	public void fetchTableKeysDescription(String tableName) throws SQLException {
		Map<Integer, List<String>> tableKeysDescriptionTemp = new HashMap<>();
		if (null == SQLQueryOptimizer.getTableKeysDescription()) {
			SQLQueryOptimizer.setTableKeysDescription(new HashMap<>());
		}
		Connection connect = null;
		RuleClient databaseClient = null;
		try {

			String tableNameTemp = tableName;
			if (tableNameTemp.contains(".")) {
				String[] table = tableNameTemp.split("\\.");
				tableNameTemp = table[table.length - 1];
			}

			Rule ruleClient = RuleFactory.create(queryText);

			databaseClient = ruleClient.databaseClient();
			connect = ((DataBaseClient) databaseClient).getConnection();
			DatabaseMetaData dataBaseMetaData = ((DataBaseClient) databaseClient)
					.getSchemaMetaData(connect);

			ResultSet primaryKeys = dataBaseMetaData.getPrimaryKeys(null, null,
					tableNameTemp);

			Integer row = 0;
			String name = null;
			StringBuilder columnsOfPrimaryKey = new StringBuilder();
			while (primaryKeys.next()) {
				name = primaryKeys.getString(6);
				columnsOfPrimaryKey
						.append(primaryKeys.getString("COLUMN_NAME")).append(
								",");
			}

			List<String> primaryKeyRow = new ArrayList<>();
			primaryKeyRow.add("Primary Key");
			primaryKeyRow.add(name);
			primaryKeyRow.add(columnsOfPrimaryKey.toString());
			primaryKeyRow.add("");
			primaryKeyRow.add("");

			tableKeysDescriptionTemp.put(row, primaryKeyRow);
			row++;

			ResultSet foreignKeys = dataBaseMetaData.getImportedKeys(null, "",
					tableName);
			List<Map<String, String>> foreignKeyList = new ArrayList<>();

			while (foreignKeys.next()) {
				Map<String, String> rowDataMap = new HashMap<>();
				List<String> rowData = new ArrayList<>();
				rowDataMap.put("FK_NAME", foreignKeys.getString("FK_NAME"));
				rowDataMap.put("FK_NAME_VALUE",
						foreignKeys.getString("FKCOLUMN_NAME"));
				rowDataMap.put("PRIMARY_TABLE", foreignKeys.getString(3));
				rowDataMap.put("PRIMARY_TABLE_KEY", foreignKeys.getString(4));
				rowData.add("Foreign Key");
				rowData.add(foreignKeys.getString("FK_NAME"));
				rowData.add(foreignKeys.getString("FKCOLUMN_NAME"));
				rowData.add(foreignKeys.getString(3));
				rowData.add(foreignKeys.getString(4));
				tableKeysDescriptionTemp.put(row, rowData);
				foreignKeyList.add(rowDataMap);
				row++;

			}
			SQLQueryOptimizer.getTableBeans().setForeignKeysDescription(
					foreignKeyList);
			SQLQueryOptimizer.getTableBeans().setPrimaryKeysDescription(
					primaryKeyRow);
			SQLQueryOptimizer.getTableKeysDescription().put(tableName,
					tableKeysDescriptionTemp);

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		} finally {
			assert databaseClient != null;
			((DataBaseClient) databaseClient).releaseConnection(connect);
		}

	}

	/**
	 * This method is used to fetch the partitions on the table
	 */
    public void fetchTablePartitions(String tableName) throws SQLException {

		if (null == SQLQueryOptimizer.getTablePartitionDescription()) {
			SQLQueryOptimizer.setTablePartitionDescription(new HashMap<>());
		}
		String tableNameTemp = tableName;
		if (tableNameTemp.contains(".")) {
			String[] table = tableNameTemp.split("\\.");
			tableNameTemp = table[table.length - 1];
		}
		String queryToExecute = "SELECT COLUMN_NAME FROM ALL_PART_KEY_COLUMNS WHERE NAME = '"
				+ tableNameTemp.toUpperCase() + "'";

        List<String> partitionKeyList = null;
        RuleClient databaseClient = null;
        ResultSet resultSets = null;
        Connection con = null;

		try {

			Rule ruleClient = RuleFactory.create(queryToExecute);

			databaseClient = ruleClient.databaseClient();
			con = ((DataBaseClient) databaseClient).getConnection();
			resultSets = ((DataBaseClient) databaseClient).executeQuery(
					queryToExecute, con);

			while (resultSets.next()) {
                if (null == partitionKeyList) {
                    partitionKeyList = new ArrayList<>();
                }
                partitionKeyList.add(resultSets.getString("COLUMN_NAME"));
            }
            SQLQueryOptimizer.getTableBeans().setPartitionKeyList(
                    partitionKeyList);
            SQLQueryOptimizer.getTablePartitionDescription().put(tableName,
                    partitionKeyList);

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		} finally {
			if (null != databaseClient) {
                assert resultSets != null;
                ((DataBaseClient) databaseClient).clearResource(resultSets.getStatement(),
                        resultSets, con);
            }
        }
    }

	/**
	 * This method is used to fetch the indexes on the table
	 */
    public void fetchTableIndexes(String tableName) throws SQLException {

		Map<String, List<String>> tableIndexesDescriptionTemp = new HashMap<>();

		if (null == SQLQueryOptimizer.getTableIndexesDescription()) {

			SQLQueryOptimizer.setTableIndexesDescription(new HashMap<>());
		}
		RuleClient databaseClient = null;
		ResultSet indexInformation = null;
		Connection con = null;
		try {
			String tableNameTemp = tableName;
			if (tableNameTemp.contains(".")) {
				String[] table = tableNameTemp.split("\\.");
				tableNameTemp = table[table.length - 1];
			}
			String queryToExecute = "SELECT AC.INDEX_NAME,AC.COLUMN_NAME  FROM ALL_IND_COLUMNS AC WHERE TABLE_NAME ='"
					+ tableNameTemp + "'";

			Rule ruleClient = RuleFactory.create(queryToExecute);

			databaseClient = ruleClient.databaseClient();
			con = ((DataBaseClient) databaseClient).getConnection();
			indexInformation = ((DataBaseClient) databaseClient).executeQuery(
					queryToExecute, con);

			List<String> columnInIndex;
			while (indexInformation.next()) {

				if (!tableIndexesDescriptionTemp.containsKey(indexInformation
						.getString("INDEX_NAME"))) {

					columnInIndex = new ArrayList<>();
					tableIndexesDescriptionTemp.put(
							indexInformation.getString("INDEX_NAME"),
							columnInIndex);

				}

				columnInIndex = tableIndexesDescriptionTemp
						.get(indexInformation.getString("INDEX_NAME"));

				columnInIndex.add(indexInformation.getString("COLUMN_NAME"));

			}
			SQLQueryOptimizer.getTableBeans().setTableIndexesDescription(
					tableIndexesDescriptionTemp);
			SQLQueryOptimizer.getTableIndexesDescription().put(tableName,
					tableIndexesDescriptionTemp);

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		} finally {
			if (null != databaseClient) {
                assert indexInformation != null;
                ((DataBaseClient) databaseClient).clearResource(indexInformation.getStatement(),
                        indexInformation, con);
            }
        }
    }

	public void setQueryText(String _queryTextLocal) {
		queryText = _queryTextLocal;
	}
}
