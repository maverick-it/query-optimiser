package com.rule.client.impl;

import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.propertiesutil.UtilityClass;
import com.rule.client.PartitionRuleClient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

/**
 * This class is Impl class for partition rule engine
 * 
 * @author 388524
 * 
 */
public class DefaultPartitionRuleClient extends PartitionRuleClient {

	public void executePartitionRuleEngine(
            Map<String, QueryBeans> _fullTableScanLocal,
            Map<String, TableInformationBeans> _tableInformationLocal, String query) {

        for (Entry<String, QueryBeans> entry : _fullTableScanLocal
                .entrySet()) {

            QueryBeans queryBean = entry.getValue();

            if (null != queryBean && null != queryBean.getExplainPlan()
                    && null != queryBean.getQuery() && null != query && queryBean.getQuery().trim().equalsIgnoreCase(query.trim())) {

                List<Map<String, Map<String, String>>> operationsList = queryBean
                        .getExplainPlan();

                if (null != operationsList) {
                    int counter = 0;
                    for (Map<String, Map<String, String>> operation : operationsList) {

                        if (operation.containsKey("TABLE ACCESS FULL")) {

                            Map<String, Map<String, String>> previousOperation = operationsList
                                    .get(counter - 1);
                            if (previousOperation
                                    .containsKey("PARTITION RANGE SINGLE")
                                    || previousOperation
                                    .containsKey("PARTITION LIST SINGLE")) {
                                counter++;
                                continue;
                            }
                            if (previousOperation
                                    .containsKey("PARTITION RANGE ALL")
                                    || previousOperation
                                    .containsKey("PARTITION LIST ALL") || previousOperation
                                    .containsKey("PARTITION LIST ITERATOR") || previousOperation
                                    .containsKey("PARTITION RANGE ITERATOR")) {
                                if ("Y".equalsIgnoreCase(UtilityClass
                                        .getSetRules("TablePartitionNotPicked"))) {
                                    OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                    violation
                                            .setMessage("msg.partitionTablePartitionNotPicked");
                                    Map<String, String> operationDetails = operation
                                            .get("TABLE ACCESS FULL");
                                    violation.setTableName(operationDetails
                                            .get("Name"));

                                    if (null != _tableInformationLocal
                                            && _tableInformationLocal
                                            .containsKey(operationDetails
                                                    .get("Name"))) {

                                        TableInformationBeans tableInfo = _tableInformationLocal
                                                .get(operationDetails
                                                        .get("Name"));

                                        List<String> partitionInfo = tableInfo
                                                .getPartitionKeyList();

                                        violation
                                                .setPartitionColumns(partitionInfo);

                                    }

                                    queryBean.setViolation(violation);
                                }
                            } else {
                                if ("Y".equalsIgnoreCase(UtilityClass
                                        .getSetRules("TableNotPartitioned"))) {
                                    OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                    violation
                                            .setMessage("msg.TableNotPartitioned");
                                    Map<String, String> operationDetails = operation
                                            .get("TABLE ACCESS FULL");
                                    violation.setTableName(operationDetails
                                            .get("Name"));
                                    queryBean.setViolation(violation);
                                }
                            }

                        } else if (operation.containsKey("PARTITION RANGE ALL")
                                || operation.containsKey("PARTITION LIST ALL")
                                || operation
                                .containsKey("PARTITION LIST ITERATOR")
                                || operation
                                .containsKey("PARTITION RANGE ITERATOR")) {
                            if ("Y".equalsIgnoreCase(UtilityClass
                                    .getSetRules("TablePartitionNotPicked"))) {
                                Map<String, Map<String, String>> nextOperation = operationsList
                                        .get(counter + 1);

                                if (nextOperation
                                        .containsKey("TABLE ACCESS FULL")) {
                                    counter++;
                                    continue;
                                }

                                OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                violation
                                        .setMessage("msg.partitionTablePartitionNotPicked");

                                Map<String, String> finalValue = null;
                                Iterator<Entry<String, Map<String, String>>> iterator = nextOperation
                                        .entrySet().iterator();
                                if (nextOperation
                                        .containsKey("INDEX FAST FULL SCAN")
                                        || nextOperation
                                        .containsKey("INDEX RANGE SCAN")) {

                                    String operationTemp = nextOperation
                                            .containsKey("INDEX FAST FULL SCAN") ? "INDEX FAST FULL SCAN"
                                            : "INDEX RANGE SCAN";
                                    Map<String, String> value = nextOperation
                                            .get(operationTemp);
                                    String indexName = value.get("Name");
                                    assert _tableInformationLocal != null;
                                    Set<String> tableNameSet = _tableInformationLocal
                                            .keySet();
                                    for (String tableName : tableNameSet) {
                                        TableInformationBeans tableInfo = _tableInformationLocal
                                                .get(tableName);
                                        if (tableInfo
                                                .getTableIndexesDescription()
                                                .containsKey(indexName)) {
                                            finalValue = new HashMap<>();
                                            finalValue.put("Name", tableName);
                                        }
                                    }

                                } else if (nextOperation
                                        .containsKey("INLIST ITERATOR")) {
                                    /**
                                     * added because table_name is not printed
                                     * in "INLIST ITERATOR" Line in query plan
                                     */
                                    Map<String, Map<String, String>> secondNextOperation = operationsList
                                            .get(counter + 2);
                                    Iterator<Entry<String, Map<String, String>>> localIterator = secondNextOperation
                                            .entrySet().iterator();
                                    String localTableName = null;
                                    while (localIterator.hasNext()) {

                                        Entry<String, Map<String, String>> locale = localIterator
                                                .next();
                                        if (null != locale.getValue()) {
                                            localTableName = locale.getValue()
                                                    .get("Name");
                                            if (!Objects.equals(localTableName, "Blank")) {
                                                break;
                                            }
                                        }
                                    }
                                    finalValue = new HashMap<>();
                                    finalValue.put("Name", localTableName);
                                } else {
                                    while (iterator.hasNext()) {

                                        Entry<String, Map<String, String>> value = iterator
                                                .next();
                                        finalValue = value.getValue();
                                    }

                                }
                                assert finalValue != null;
                                violation.setTableName(finalValue.get("Name"));
                                String Value = finalValue.get("Name");
                                if (null != _tableInformationLocal) {
                                    Set<String> tableNameSet = _tableInformationLocal
                                            .keySet();
                                    for (String tableName : tableNameSet) {
                                        if (null != tableName
                                                && tableName.contains(".")
                                                && tableName.contains(Value)) {
                                            String[] table = tableName
                                                    .split("\\.");
                                            if (table[table.length - 1]
                                                    .equalsIgnoreCase(finalValue
                                                            .get("Name"))) {
                                                Value = tableName;
                                            }
                                        }
                                    }
                                }

                                if (null != _tableInformationLocal
                                        && _tableInformationLocal
                                        .containsKey(Value)) {

                                    TableInformationBeans tableInfo = _tableInformationLocal
                                            .get(Value);

                                    List<String> partitionInfo = tableInfo
                                            .getPartitionKeyList();

                                    violation
                                            .setPartitionColumns(partitionInfo);

                                }
                                if (null != violation.getPartitionColumns()
                                        && !"Blank".equalsIgnoreCase(violation
                                        .getTableName())) {
                                    queryBean.setViolation(violation);
                                }

                            }
                        }
                        counter++;
                    }
                }
            }
        }
    }

    public void checkIsTablePartitioned(
            Map<String, QueryBeans> _fullTableScanLocal,
            Map<String, TableInformationBeans> _tableInformationLocal,
            String query) {
        if (!"INSERT".equalsIgnoreCase(query)) {
			Set<String> tableNameSet = _tableInformationLocal.keySet();
			Iterator<Entry<String, QueryBeans>> queryDetails = _fullTableScanLocal
					.entrySet().iterator();
			if (queryDetails.hasNext()) {
				Entry<String, QueryBeans> entry = queryDetails.next();
				QueryBeans queryBean = entry.getValue();

				for (String tableName : tableNameSet) {
					TableInformationBeans tableInfo = _tableInformationLocal
							.get(tableName);
                    if ((null == tableInfo.getPartitionKeyList() || tableInfo
                            .getPartitionKeyList().isEmpty())
                            && !"DUAL".equalsIgnoreCase(tableName)) {
                        OptimizeSuggestBean violation = new OptimizeSuggestBean();
                        violation.setMessage("msg.TableNotPartitioned");
                        violation.setTableName(tableName);
                        queryBean.setViolation(violation);
                    }
                }
			}
		}
	}

}
