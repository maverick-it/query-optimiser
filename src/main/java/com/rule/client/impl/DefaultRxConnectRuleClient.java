package com.rule.client.impl;

import com.bean.ColumnDetailsBean;
import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.propertiesutil.UtilityClass;
import com.rule.client.RxConnectRuleClient;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


/**
 * @author 388524
 * 
 */
public class DefaultRxConnectRuleClient extends RxConnectRuleClient {

	public void executeRxConnectRules(
            Map<String, QueryBeans> _fullTableScanLocal,
            Map<String, TableInformationBeans> _tableInformationLocal) {

        for (Entry<String, QueryBeans> entry : _fullTableScanLocal
                .entrySet()) {

            QueryBeans queryBean = entry.getValue();

            String nullCheckColumnNames = UtilityClass
                    .getRule("rule.NullCheckColumnsList");

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("ConsiderPlacingNullCheck"))) {
                executeRuleCheckOnColumns(nullCheckColumnNames, queryBean,
                        _tableInformationLocal, false,
                        false);
            }

            String dirtyColumns = UtilityClass.getRule("rule.dirtyColumnsList");

            if (!"UPDATE".equalsIgnoreCase(queryBean.getStatementType())) {

                if ("Y".equalsIgnoreCase(UtilityClass
                        .getSetRules("ConsiderIsDirtyColCheck"))) {
                    executeRuleCheckOnColumns(dirtyColumns, queryBean,
                            _tableInformationLocal, true,
                            false);
                }
            }

            if ("UPDATE".equalsIgnoreCase(queryBean.getStatementType())) {

                String auditColumns = UtilityClass
                        .getRule("rule.auditedColumnsList");

                if ("Y".equalsIgnoreCase(UtilityClass
                        .getSetRules("ColumnToIncludeInUpdateQuery"))) {
                    executeRuleCheckOnColumns(auditColumns, queryBean,
                            _tableInformationLocal, false,
                            true);
                }

            }


            executeDynamicCommaRule(queryBean,
                    _tableInformationLocal);

        }

	}

    private void executeRuleCheckOnColumns(String columnNames,
                                           QueryBeans _queryBeanLocal,
                                           Map<String, TableInformationBeans> _tableInformationLocal,
                                           boolean isCheckDirty, boolean isAuditColumn) {

        boolean columnFound;

        String[] columns = columnNames.split(",");

        for (String columnNameRule : columns) {

			columnFound = false;
			
			columnNameRule = columnNameRule.toUpperCase();

			Map<String, String> aliasTableMap = _queryBeanLocal.getAliasMap();

			if (null != aliasTableMap) {

                for (Entry<String, String> keyValueDetails : aliasTableMap
                        .entrySet()) {

                    String key = keyValueDetails.getKey();

                    Set<String> columnsSet = null;
                    Map<String, List<Object>> columnsLocal = null;
                    if (_tableInformationLocal.containsKey(key)) {

                        TableInformationBeans tableInfo = _tableInformationLocal
                                .get(key);

                        if (null != tableInfo) {

                            columnsLocal = tableInfo.getTableColumnDescription();
                            columnsSet = columnsLocal.keySet();
                        }
                    }

                    if (null != columnsSet
                            && columnsSet.contains(columnNameRule)) {

                        Map<String, String> aliasMap = _queryBeanLocal
                                .getAliasMap();
                        String alias = aliasMap.get(key);
                        String columnNameForTableInQuery;
                        if (null != alias && !aliasMap.isEmpty()
                                && aliasMap.size() > 1) {
                            columnNameForTableInQuery = alias + "."
                                    + columnNameRule;
                        } else {
                            columnNameForTableInQuery = columnNameRule;
                        }

                        List<ColumnDetailsBean> columnList = isAuditColumn ? _queryBeanLocal
                                .getSelectColumnName() : _queryBeanLocal
                                .getWhereClauseColumnList();

                        if (null != columnList) {

                            for (ColumnDetailsBean cb : columnList) {

                                String columnName = cb.getColumnName();

                                if (columnName
                                        .contains(columnNameForTableInQuery)) {

                                    columnFound = true;
                                    break;
                                }

                            }

                        }

                        if (!columnFound && null != columnsLocal) {

                            List<Object> columnProperties = columnsLocal
                                    .get(columnNameRule);

                            if (!"0".equals(columnProperties
                                    .get(columnProperties.size() - 2))) {

                                OptimizeSuggestBean violation = new OptimizeSuggestBean();

                                if (isAuditColumn) {

                                    violation
                                            .setMessage("msg.columnToIncludeInUpdateQuery");

                                } else if (!isCheckDirty) {

                                    violation
                                            .setMessage("msg.warning.considerPlacingNullCheck");
                                } else {

                                    violation
                                            .setMessage("msg.warning.considerIsDirtyColCheck");
                                }

                                violation.setTableName(key);

                                violation.setColumnName(columnNameRule);

                                _queryBeanLocal.setViolation(violation);
                            }

                        }

                    }

                }
            }
        }

	}

    private void executeDynamicCommaRule(QueryBeans queryBean,
                                         Map<String, TableInformationBeans> _tableInformationLocal) {
        if (null != queryBean && null != queryBean.getWhereClauseColumnList()) {
            Set<String> tableNameSet = _tableInformationLocal.keySet();
            List<ColumnDetailsBean> columnList = queryBean
                    .getWhereClauseColumnList();
            columnList.stream().filter(cb -> "IN".equalsIgnoreCase(cb.getOperator()) && cb.getColumnValue().contains("COMMON_PKG")).forEach(cb -> {
                for (String tableName : tableNameSet) {
                    TableInformationBeans tableInfo = _tableInformationLocal
                            .get(tableName);
                    Map<String, List<Object>> colDetails = tableInfo
                            .getTableColumnDescription();
                    String[] colName = cb.getColumnName().split("\\.");
                    int length = colName.length;
                    if (colDetails.containsKey(colName[length - 1])) {
                        List<Object> colDescription = colDetails
                                .get(colName[length - 1]);
                        if (colDescription.get(3).toString()
                                .contains("VARCHAR")) {
                            if (cb.getColumnValue().contains(
                                    "COMMON_PKG.F_IN_LIST_NUM")) {
                                OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                violation.setMessage("msg.Use.F_IN_LIST");
                                violation.setTableName(colName[length - 1]);
                                queryBean.setViolation(violation);
                            }
                        } else if (colDescription.get(3).toString()
                                .contains("NUMBER")) {
                            if (!cb.getColumnValue().contains(
                                    "COMMON_PKG.F_IN_LIST_NUM")) {
                                OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                violation
                                        .setMessage("msg.Use.F_IN_LIST_NUM");
                                violation.setTableName(colName[length - 1]);
                                queryBean.setViolation(violation);
                            }
                        }
                    }
                }
            });
        }
    }
}
