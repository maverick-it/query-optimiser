package com.rule.client.impl;

import com.bean.ColumnDetailsBean;
import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.optimiser.SQLQueryOptimizer;
import com.propertiesutil.UtilityClass;
import com.rule.client.DataBaseClient;
import com.rule.client.GeneralRuleClient;
import com.rule.client.Rule;
import com.rule.client.RuleClient;
import com.rule.client.RuleFactory;
import com.util.LoggerUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * This class is Impl class for General Rules engine
 * 
 * @author 388524
 * 
 */
public class DefaultGeneralRuleClient extends GeneralRuleClient {

    private static void populateAccessAndFilters(String currentRow) {
        if (currentRow.contains("access")) {
            SQLQueryOptimizer.getAccessOperations().add(currentRow);
        } else if (currentRow.contains("filter")) {
            SQLQueryOptimizer.getFilterOperations().add(currentRow);
        }

    }

    private static void formulateCurrentOperation(String currentRow) {

        String temp = currentRow;
        temp = temp.trim();
        if (temp.length() == 0) {
            return;
        }

        if (currentRow.contains("Id")) {
            StringTokenizer tokens = new StringTokenizer(currentRow, "|");

            while (tokens.hasMoreTokens()) {
                String operationName = tokens.nextToken();
                operationName = operationName.trim();
                SQLQueryOptimizer.getListOfHeadings().add(operationName);
            }

        } else if (!currentRow.contains("hash")
                && !currentRow.contains("Predicate Information")) {
            StringTokenizer currentToken = new StringTokenizer(currentRow, "|");

            String operationName = null;
            String orderNumber = null;
            int tempIndex;

            for (int index = 0; index < SQLQueryOptimizer.getListOfHeadings()
                    .size(); index++) {
                tempIndex = index;

                if (SQLQueryOptimizer.getListOfHeadings().get(index)
                        .equalsIgnoreCase("id")
                        && currentToken.hasMoreTokens()) {
                    orderNumber = currentToken.nextToken();
                    orderNumber = orderNumber.trim();
                } else if (SQLQueryOptimizer.getListOfHeadings().get(index)
                        .equalsIgnoreCase("operation")
                        && currentToken.hasMoreTokens()) {
                    operationName = currentToken.nextToken();

                    operationName = operationName.trim();

                    if (operationName.length() == 0) {
                        operationName = "Blank";
                    }

                    SQLQueryOptimizer
                            .setOperationMap(new HashMap<>());

                    SQLQueryOptimizer.getOperationMap().put(operationName,
                            new HashMap<>());

                    SQLQueryOptimizer
                            .getOperationMap()
                            .get(operationName)
                            .put(SQLQueryOptimizer.getListOfHeadings().get(0),
                                    orderNumber);

                } else {
                    while (currentToken.hasMoreTokens()) {
                        String current = currentToken.nextToken();
                        current = current.trim();

                        if (current.length() == 0) {
                            current = "Blank";
                        }

                        SQLQueryOptimizer
                                .getOperationMap()
                                .get(operationName)
                                .put(SQLQueryOptimizer.getListOfHeadings().get(
                                        tempIndex), current);
                        tempIndex = tempIndex + 1;
                        index = tempIndex;
                    }
                    SQLQueryOptimizer.getListOfOperations().add(
                            SQLQueryOptimizer.getOperationMap());
                }

			}

		}

	}

    private static String formatQuery(String query) {
        query = query.replace(System.getProperty("line.separator"),
                "");
        LoggerUtil.performLogging(query);
        return query;

	}

    public void fetchQueryPlan(Map<String, QueryBeans> fullTableScan)
            throws Exception {

        ResultSet result = null;
        Connection con = null;
        RuleClient databaseClient = null;
       
        try {

            if (null != fullTableScan) {

                for (Entry<String, QueryBeans> entry : fullTableScan
                        .entrySet()) {

                    QueryBeans queryBean = entry.getValue();

                    if (null != queryBean && null != queryBean.getQuery()) {

                        String query = queryBean.getQuery();

                        query = formatQuery(query);

                        Rule rules = RuleFactory.create(query);

                        databaseClient = rules.databaseClient();
                        
                        if (query.startsWith("(")) {
                            int queryLength = query.length();
                            query = (String) query.subSequence(1,
                                    queryLength - 1);
                        }
                        
                        con = ((DataBaseClient)databaseClient).getConnection();
                        result = ((DataBaseClient)databaseClient).executeQueryPlan(query, con);
                        
                        if (null != result) {
                            while (result.next()) {

                                String currentRow = result.getString(1);
                                LoggerUtil.performLogging(currentRow);

                                if (!currentRow.startsWith("-")
                                        && currentRow.length() > 0) {
                                    if (currentRow
                                            .contains("Predicate Information")
                                            || currentRow
                                            .equalsIgnoreCase("Note")) {
                                        SQLQueryOptimizer.getListOfHeadings()
                                                .clear();
                                        break;
                                    }
                                    if (currentRow.contains("access")
                                            || currentRow.contains("filter")) {
                                        populateAccessAndFilters(currentRow);
                                    } else {
                                        formulateCurrentOperation(currentRow);
                                    }
                                }

                            }

                            queryBean.setExplainPlanDetails(SQLQueryOptimizer
                                    .getListOfOperations());

                            SQLQueryOptimizer
                                    .setListOfOperations(new ArrayList<>());
                            SQLQueryOptimizer
                                    .setListOfHeadings(new ArrayList<>());
                        }
                    }
                }
            }
        } finally {

            if (null != databaseClient) {
                assert result != null;
                ((DataBaseClient) databaseClient).clearResource(result.getStatement(), result, con);
            }
        }

	}

    public void executeGeneralRuleEngine(String queryText,
                                         Map<String, QueryBeans> _fullTableScanLocal,
                                         Map<String, TableInformationBeans> _tableInformationLocal) {

        for (Entry<String, QueryBeans> entry : _fullTableScanLocal
                .entrySet()) {

            QueryBeans queryBean = entry.getValue();

            boolean isAllColumnRuleFailed = executeCountColumnRule(queryText, queryBean,
                    _tableInformationLocal);

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("UseUNIONALLinsteadOfUseUnion"))) {

                executeUnionAllRule(queryText, queryBean
                );
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("useWHEREInsteadOfHaving"))) {

                executeWhereRule(queryBean
                );
            }

            if (!isAllColumnRuleFailed) {


                executeUseLessTableRule(queryBean,
                        _tableInformationLocal);
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("UseExistsInsteadOfNotIn"))) {

                executeExistsOverNotInRule(queryBean
                );
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("DonotUseDistinct"))) {

                executeDistinctRule(queryBean
                );
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("DoNotUseOrderByClause"))) {

                executeOrderByRule(queryBean
                );
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("DoNotLikeOperator"))) {

                executeLikeRule(queryBean
                );
            }

            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("UseNonNullableColumn"))) {

                executeInsertRule(queryBean, _fullTableScanLocal,
                        _tableInformationLocal);
            }
            if ("Y".equalsIgnoreCase(UtilityClass
                    .getSetRules("HardParsing"))
                    && queryText.trim().equalsIgnoreCase(
                    queryBean.getQuery().trim())) {
                executeHardParsingRule(queryBean
                );
            }
        }

	}

    private void executeExistsOverNotInRule(QueryBeans _queryBeanLocal) {
        if (null != _queryBeanLocal
                && null != _queryBeanLocal.getWhereClauseColumnList()) {

			List<ColumnDetailsBean> beanList = _queryBeanLocal
			.getWhereClauseColumnList();

            beanList.stream().filter(bean -> "NOT IN".equalsIgnoreCase(bean.getOperator())).forEach(bean -> {
                OptimizeSuggestBean violation = new OptimizeSuggestBean();
                violation
                        .setMessage("msg.warning.useExistsInsteadOfNotIn");
                _queryBeanLocal.setViolation(violation);
            });

        }

	}

    private void executeUseLessTableRule(QueryBeans _queryBeanLocal,
                                         Map<String, TableInformationBeans> _tableInformationLocal) {

		if (null != _queryBeanLocal
                && "SELECT".equalsIgnoreCase(_queryBeanLocal.getStatementType())
                && null != _queryBeanLocal.getAliasMap()) {

			Map<String, String> aliasTableMap = _queryBeanLocal.getAliasMap();

            Set<String> columnSetUpdatedWithAlias = new HashSet<>();
            Set<String> columnSetUpdatedWithoutAlias = new HashSet<>();
            String[] columns = (UtilityClass.getRule("rule.AggregateFunctions"))
                    .split(",");
            boolean isSingleTableQuery = false;

            if (aliasTableMap.size() == 1) {

                isSingleTableQuery = true;
            }

            for (Entry<String, String> keyValueDetails : aliasTableMap
                    .entrySet()) {

                String key = keyValueDetails.getKey();
                String alias = keyValueDetails.getValue();
                Set<String> columnsSet = null;

                if (_tableInformationLocal.containsKey(key)) {

                    TableInformationBeans tableInfo = _tableInformationLocal
                            .get(key);

                    if (null != tableInfo) {

                        Map<String, List<Object>> columnsLocal = tableInfo
                                .getTableColumnDescription();
                        columnsSet = columnsLocal.keySet();
                    }
                }

                if (null != alias && null != columnsSet) {

                    columnSetUpdatedWithAlias = new HashSet<>();
                    columnSetUpdatedWithoutAlias = new HashSet<>();

                    if (isSingleTableQuery) {

                        columnSetUpdatedWithoutAlias.addAll(columnsSet);
                    }

                    for (String columnName : columnsSet) {

                        columnName = alias + "." + columnName;

                        columnSetUpdatedWithAlias.add(columnName);

                    }
                } else if (null != columnsSet) {

                    columnSetUpdatedWithAlias = new HashSet<>();
                    columnSetUpdatedWithAlias.addAll(columnsSet);
                }

                boolean columnFound = false;
                boolean foundInJoinOnly = false;
                if (null != _queryBeanLocal.getSelectColumnName()) {

                    List<ColumnDetailsBean> colBean = _queryBeanLocal
                            .getSelectColumnName();

                    for (ColumnDetailsBean cb : colBean) {

                        String columnName = cb.getColumnName();

                        if (columnSetUpdatedWithAlias.contains(columnName)) {

                            columnFound = true;
                            break;
                        } else if (isSingleTableQuery
                                && columnSetUpdatedWithoutAlias
                                .contains(columnName)) {

                            columnFound = true;
                            break;
                        }

                        if (isSingleTableQuery && columns.length > 0) {

                            for (String function : columns) {

                                if (columnName.contains(function + "(")) {
                                    columnFound = true;
                                    break;
                                }
                            }
                        }

                        /*
                         * else { for (String colName :
                         * columnSetUpdatedWithAlias) { if
                         * (columnName.contains(colName)) { columnFound =
                         * true; break; } } if (columnFound) { break; } }
                         */
                    }
                }

                if (!columnFound) {

                    if (null != _queryBeanLocal.getWhereClauseColumnList()) {

                        List<ColumnDetailsBean> colBean = _queryBeanLocal
                                .getWhereClauseColumnList();

                        for (ColumnDetailsBean cb : colBean) {

                            String columnName = cb.getColumnName();

                            if (null != columnName
                                    && columnName.contains("(")
                                    && columnName.contains(")")) {

                                int startIndex = columnName.indexOf("(");
                                int endIndex = columnName.lastIndexOf(")");

                                if (startIndex >= 0 && endIndex >= 0) {

                                    columnName = columnName.substring(
                                            startIndex + 1, endIndex);
                                }

                                String[] values = columnName.split(",");

                                columnName = values[0];

                            }
                            String rsColumn = cb.getColumnValue();

                            /*
                             * if (null != rsColumn &&
                             * rsColumn.contains("(") &&
                             * rsColumn.contains(")")) {
                             * 
                             * int startIndex = rsColumn.indexOf("("); int
                             * endIndex = rsColumn.lastIndexOf(")");
                             * 
                             * if (startIndex >= 0 && endIndex >= 0) {
                             * 
                             * rsColumn = rsColumn.substring( startIndex +
                             * 1, endIndex); }
                             * 
                             * String[] values = rsColumn.split(",");
                             * 
                             * rsColumn = values[0];
                             * 
                             * }
                             */
                            String tempCol = "";
                            if (null != rsColumn) {
                                String[] columnNameTemp = rsColumn
                                        .split("\\.");
                                if (columnNameTemp.length > 1) {
                                    tempCol = columnNameTemp[1];
                                } else {
                                    tempCol = columnNameTemp[0];
                                }
                            }

                            for (String columnSet : columnSetUpdatedWithAlias) {

                                if ((columnName != null && columnName.contains(columnSet))
                                        && !"".equals(tempCol)
                                        && !columnName.contains(tempCol)) {

                                    columnFound = true;
                                    foundInJoinOnly = false;
                                    break;
                                }

                                assert rsColumn != null;
                                assert columnName != null;
                                if (rsColumn.contains(columnSet)
                                        || (columnName.contains(columnSet)
                                        && !"".equals(tempCol) && columnName
                                        .contains(tempCol))) {

                                    foundInJoinOnly = true;
                                    columnFound = true;
                                }
                            }

                            for (String columnSet : columnSetUpdatedWithoutAlias) {

                                assert columnName != null;
                                if (columnName.contains(columnSet)
                                        && !"".equals(tempCol)
                                        && !columnName.contains(tempCol)) {

                                    columnFound = true;
                                    foundInJoinOnly = false;
                                    break;
                                }

                                assert rsColumn != null;
                                if (rsColumn.contains(columnSet)
                                        || (columnName.contains(columnSet)
                                        && !"".equals(tempCol) && columnName
                                        .contains(tempCol))) {

                                    foundInJoinOnly = true;
                                    columnFound = true;
                                }
                            }

                            if (columnFound) {
                                break;
                            }
                        }

                    }
                }

                if (!columnFound && null != columnsSet
                        && !key.equalsIgnoreCase("DUAL")) {

                    if ("Y".equalsIgnoreCase(UtilityClass
                            .getSetRules("UselessTableInQuery"))) {
                        OptimizeSuggestBean violation = new OptimizeSuggestBean();
                        violation.setMessage("msg.uselessTableInQuery");
                        violation.setTableName(key);
                        _queryBeanLocal.setViolation(violation);
                    }

                }

                if (foundInJoinOnly) {

                    if ("Y".equalsIgnoreCase(UtilityClass
                            .getSetRules("TableFoundToBeUsedInJoinOnly"))) {

                        OptimizeSuggestBean violation = new OptimizeSuggestBean();
                        violation
                                .setMessage("msg.warning.tableFoundToBeUsedInJoinOnly");
                        violation.setTableName(key);
                        _queryBeanLocal.setViolation(violation);
                    }

                }

            }

        }
    }

    private boolean executeCountColumnRule(String queryText, QueryBeans queryBean,
                                           Map<String, TableInformationBeans> _tableInformationLocal) {

		boolean isAllColumnRuleFailed = false;
        if (null != queryBean && null != queryBean.getSelectColumnName()) {

            List<ColumnDetailsBean> beanList = queryBean.getSelectColumnName();

            for (ColumnDetailsBean bean : beanList) {

                if (!bean.getColumnName().contains("COUNT")
                        && bean.getColumnName().equals("*")
                /* && !bean.getColumnName().contains(".*") */) {
                    if (queryText.trim().equalsIgnoreCase(
                            queryBean.getQuery().trim())
                            && "Y".equalsIgnoreCase(UtilityClass
                            .getSetRules("DonotSelectAllColumn"))) {
                        OptimizeSuggestBean violation = new OptimizeSuggestBean();
                        violation.setMessage("msg.donotSelectAllCol");

                        queryBean.setViolation(violation);
                    }
                    isAllColumnRuleFailed = true;

                }
                if (!isAllColumnRuleFailed
                        && !bean.getColumnName().contains("COUNT")
                        && bean.getColumnName().contains("*")) {
                    Set<String> tableSet = _tableInformationLocal.keySet();
                    for (String table : tableSet) {
                        String allCol = table + ".*";
                        if (bean.getColumnName().equalsIgnoreCase(
                                allCol)) {
                            if ("Y".equalsIgnoreCase(UtilityClass
                                    .getSetRules("DonotSelectAllColumn"))) {
                                OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                violation
                                        .setMessage("msg.donotSelectAllCol");

                                queryBean.setViolation(violation);
                            }
                            isAllColumnRuleFailed = true;
                            break;
                        }
                        if (null != _tableInformationLocal
                                .get(table)) {
                            List<String> aliasList = _tableInformationLocal
                                    .get(table).getAliasName();
                            if (null != aliasList) {
                                for (String aliasName : aliasList) {
                                    String alias = aliasName + ".*";
                                    if (bean.getColumnName()
                                            .equalsIgnoreCase(alias)) {
                                        if ("Y".equalsIgnoreCase(UtilityClass
                                                .getSetRules("DonotSelectAllColumn"))) {
                                            OptimizeSuggestBean violation = new OptimizeSuggestBean();
                                            violation
                                                    .setMessage("msg.donotSelectAllCol");

                                            queryBean
                                                    .setViolation(violation);
                                        }
                                        isAllColumnRuleFailed = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (isAllColumnRuleFailed) {
                            break;
                        }

                    }
                }

                if (bean.getColumnName().contains("COUNT")
                        && bean.getColumnName().contains("*")
                        && !bean.getColumnName().contains("OVER")) {

                    if ("Y".equalsIgnoreCase(UtilityClass
                            .getSetRules("DonotUseCountAll"))) {
                        OptimizeSuggestBean violation = new OptimizeSuggestBean();
                        violation.setMessage("msg.donotUseCountAll");

                        queryBean.setViolation(violation);
                    }
                }

            }

        }
        return isAllColumnRuleFailed;
    }

    private void executeUnionAllRule(String queryText, QueryBeans queryBean) {

        if (null != queryBean && null != queryBean.getSelectColumnName()) {
            if (queryText.contains("UNION") && !queryText.contains("UNION ALL")) {
                OptimizeSuggestBean violation = new OptimizeSuggestBean();
				violation.setMessage("msg.useUNIONALLinsteadOfUseUnion");
				queryBean.setViolation(violation);
			}
		}
	}


    private void executeDistinctRule(QueryBeans queryBean) {

        if (null != queryBean && null != queryBean.getSelectColumnName()) {
            String query = queryBean.getQuery();
            if (null != query) {
				if (query.contains("DISTINCT")) {
					OptimizeSuggestBean violation = new OptimizeSuggestBean();
					violation.setMessage("msg.donotUseDistinct");
					queryBean.setViolation(violation);
				}
			}

		}
	}

    private void executeWhereRule(QueryBeans queryBean) {

        if (null != queryBean && null != queryBean.getSelectColumnName()) {
            String query = queryBean.getQuery();
            if (null != query) {
				if (query.contains("HAVING")) {
					OptimizeSuggestBean violation = new OptimizeSuggestBean();
					violation.setMessage("msg.useWHEREInsteadOfHaving");

					queryBean.setViolation(violation);
				}
			}

		}
	}

    private void executeOrderByRule(QueryBeans queryBean) {

        if (null != queryBean && null != queryBean.getSelectColumnName()) {
            String query = queryBean.getQuery();
            if (null != query) {
				if (query.contains("ORDER BY")) {
					OptimizeSuggestBean violation = new OptimizeSuggestBean();
					violation.setMessage("msg.doNotUseOrderByClause");
					queryBean.setViolation(violation);
				}
			}

		}
	}

    private void executeLikeRule(QueryBeans queryBean) {

        if (null != queryBean && null != queryBean.getSelectColumnName()) {
            String query = queryBean.getQuery();
            if (null != query) {
				if (query.contains("LIKE")) {
					if (query.contains("'%")) {
						OptimizeSuggestBean likeViolation = new OptimizeSuggestBean();
						likeViolation.setMessage("msg.Use%afterName");
						queryBean.setViolation(likeViolation);
					} else {
						OptimizeSuggestBean violation = new OptimizeSuggestBean();
						violation.setMessage("msg.doNotLikeOperator");
						queryBean.setViolation(violation);
					}
				}
			}

		}
	}

    private void executeInsertRule(QueryBeans queryBean,
                                   Map<String, QueryBeans> _fullTableScanLocal,
                                   Map<String, TableInformationBeans> _tableInformationLocal) {
        if (null != queryBean && null != queryBean.getStatementType()
                && "INSERT".equalsIgnoreCase(queryBean.getStatementType())) {
            Set<String> keySet = _tableInformationLocal.keySet();
            int isNotNullable = 0;
			for (String tableName : keySet) {
				TableInformationBeans tableData = _tableInformationLocal
				.get(tableName);
				Set<String> columnSet = tableData.getTableColumnDescription()
				.keySet();
				for (String columnName : columnSet) {
					if (null != tableData.getTableColumnDescription()
							.get(columnName).get(4)
							&& tableData.getTableColumnDescription()
							.get(columnName).get(4)
							.equals(isNotNullable)) {
						if (null != queryBean.getQuery()
								&& null != _fullTableScanLocal.get(queryBean
										.getQuery())
										&& null != _fullTableScanLocal.get(
												queryBean.getQuery())
                                .getSelectColumnName()) {
                            List<ColumnDetailsBean> insertedColumnName = _fullTableScanLocal
                                    .get(queryBean.getQuery())
                                    .getSelectColumnName();
                            boolean columnFound = false;
                            for (ColumnDetailsBean insertName : insertedColumnName) {
								if (insertName.getColumnName().contains(
										columnName)) {
									columnFound = true;
									break;
								}
							}
							if (!columnFound) {
								OptimizeSuggestBean violation = new OptimizeSuggestBean();
								violation
								.setMessage("msg.UseNonNullableColumn");
								violation.setTableName(columnName);
								violation.setColumnName(columnName);
								queryBean.setViolation(violation);
							}
						}
					}
				}
			}
		}
	}

	public void executeMergerCartesianRule(
            Map<String, QueryBeans> _fullTableScanLocal,
            Map<String, TableInformationBeans> _tableInformationLocal,
            String query) {
        for (Entry<String, QueryBeans> entry : _fullTableScanLocal
                .entrySet()) {

            QueryBeans queryBean = entry.getValue();

            if (null != queryBean
                    && null != queryBean.getExplainPlan()
                    && null != queryBean.getQuery()
                    && null != query
                    && queryBean.getQuery().trim()
                    .equalsIgnoreCase(query.trim())) {
                List<Map<String, Map<String, String>>> operationsList = queryBean
                        .getExplainPlan();

                if (null != operationsList) {
                    for (Map<String, Map<String, String>> operation : operationsList) {

                        if (operation.containsKey("MERGE JOIN CARTESIAN")) {
                            OptimizeSuggestBean violation = new OptimizeSuggestBean();
                            violation.setMessage("msg.MergeCartesianRule");
                            queryBean.setViolation(violation);
                            break;
                        }
                    }
                }
            }
        }
    }

    private void executeHardParsingRule(QueryBeans queryBean) {
        if (null != queryBean && null != queryBean.getWhereClauseColumnList()) {
            List<ColumnDetailsBean> columnList = queryBean
                    .getWhereClauseColumnList();
            for (ColumnDetailsBean cb : columnList) {
                if ("IN".equalsIgnoreCase(cb.getOperator())) {
                    int counterComma = 0;
					String colValue = cb.getColumnValue();
					int length = colValue.length();
					for (int i = 0; i < length; i++) {
						if (colValue.charAt(i) == ',') {
							counterComma++;
						}
					}
					if (counterComma == 0 && !colValue.contains("COMMON_PKG")) {
						OptimizeSuggestBean violation = new OptimizeSuggestBean();
						violation.setMessage("msg.HardParsing");
						queryBean.setViolation(violation);
					}
				}
			}
		}
	}

}
