package com.rule.client.impl;

import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.propertiesutil.UtilityClass;
import com.rule.client.DataBaseClient;
import com.util.LoggerUtil;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class DefaultDataBaseClient extends DataBaseClient {

	public ResultSet executeQuery(String queryToExecute, Connection connection) {

        Statement statement;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(queryToExecute);
			result = ((PreparedStatement) statement).executeQuery();

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		}

		return result;

	}

	public void clearResource(Statement _statementLocal,
			ResultSet _resultLocal, Connection _connectionLocal) {

		try {
			if (null != _statementLocal) {
				_statementLocal.close();
			}
			if (null != _resultLocal) {
				_resultLocal.close();
			}

			releaseConnection(_connectionLocal);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub

	}

	public DatabaseMetaData getSchemaMetaData(Connection _connectLocal)
			throws SQLException {

		return _connectLocal.getMetaData();
	}

	public void releaseConnection(Connection connect) throws SQLException {

		if (null != connect) {

			try {
				connect.close();
			} catch (SQLException sq) {
				connect.close();
			} finally {
				LoggerUtil.performLogging(this.getClass().getName(), "releaseConnection",
                        "Connection Closed Successfully");
            }
        }
    }

	public ResultSet executeQueryPlan(String queryToExecute, Connection connect) {

		ResultSet result = null;
        Statement statement;

		try {

			statement = connect.createStatement();
			statement.execute("EXPLAIN PLAN FOR " + queryToExecute);

			result = statement
					.executeQuery("SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY())");

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
		}

		return result;

	}

	public boolean executeQueryPlanOnMainQuery(
			Map<String, QueryBeans> fullTableScan, String queryToExecute) {

		boolean isValidQuery = true;
		Statement statement = null;
		Connection connect = null;

		try {
			connect = getConnection();
			statement = connect.createStatement();
			statement.execute("EXPLAIN PLAN FOR " + queryToExecute);

			statement
					.executeQuery("SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY())");

		} catch (SQLException e) {
			System.out
					.print("Either table does not exist or you don't have rights to get this information");
			System.out.println(e.getMessage());
			Iterator<Entry<String, QueryBeans>> iterator = fullTableScan
					.entrySet().iterator();
			Entry<String, QueryBeans> entry = iterator.next();
			QueryBeans queryBean = entry.getValue();
			OptimizeSuggestBean violation = new OptimizeSuggestBean();
			violation.setMessage("msg.QueryFailedToExecute");
			violation.setTableName(e.getMessage());
			queryBean.setViolation(violation);
			isValidQuery = false;
		} finally {
			clearResource(statement, null, connect);
		}

		return isValidQuery;

	}

	public Connection getConnection() {

		Connection connection = null;
		try {

			String driverName = UtilityClass
					.getProperties("configuration.driverName");
			String serverName = UtilityClass
					.getProperties("configuration.serverName");
			String portNumber = UtilityClass
					.getProperties("configuration.portNumber");
			String dataBaseName = UtilityClass
					.getProperties("configuration.dataBaseName");
			String username = UtilityClass
					.getProperties("configuration.username");
			String password = UtilityClass
					.getProperties("configuration.password");

			Class.forName(driverName);

			String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber
					+ ":" + dataBaseName;
			connection = DriverManager.getConnection(url, username, password);

		} catch (ClassNotFoundException | SQLException e) {
			LoggerUtil.performLogging(e.getMessage());
			e.printStackTrace();
		}
		return connection;
	}

}
