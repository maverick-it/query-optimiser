package com.rule.client;

/**
 * A marker interface to denote all the rule clients. Different types of rule
 * clients will be implementing the corresponding abstract classes
 * 
 * Any new client created should implement this interface maintaining the
 * corresponding abstract-class hierarchy
 */
public interface RuleClient {

}
