package com.rule.client;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.rule.client.impl.DefaultDataBaseClient;

/**
 * The abstract class declares the GeneralDataBaseClient. The corresponding
 * implementation can be found at {@link DefaultDataBaseClient}
 */
public abstract class DataBaseClient implements RuleClient{

    public abstract ResultSet executeQuery(String queryToExecute, Connection _connectionLocal);

	public abstract DatabaseMetaData getSchemaMetaData(Connection _connectLocal) throws SQLException;
	
	public abstract void releaseConnection(Connection connect) throws SQLException;

    public abstract ResultSet executeQueryPlan(String queryToExecute, Connection _connectionLocal);

	public abstract Connection getConnection();
	
	public abstract void clearResource(Statement _statementLocal,
			ResultSet _resultLocal, Connection _connectionLocal) ;

}
