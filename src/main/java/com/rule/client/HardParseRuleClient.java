package com.rule.client;


import java.util.Map;

public abstract class HardParseRuleClient implements RuleClient{

    public abstract Map<String, String> executeHardParseRule(
            String query);
}
