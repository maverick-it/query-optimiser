package com.util;

import com.bean.TableInformationBeans;
import com.optimiser.Optimizer;
import com.parser.util.SQLParserUtil;
import com.rule.client.RuleClient;
import com.rule.client.TableDetailsClient;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeanInitializationUtil {

	public static boolean initializeTableInformation(
			RuleClient tableDetailsClient) throws SQLException {
		boolean isTableExists = false;
		List<String> tableList = Optimizer.getTableNamesList();

		for (String tableNameTemp : tableList) {

			isTableExists = isTableExists(tableDetailsClient, tableNameTemp);

			if (isTableExists) {
				populateTableInfoBeans(tableDetailsClient, tableNameTemp);
			} else {
				ViolationUtil.reportTableNotExistsIssue();
				break;
			}
		}
		return isTableExists;
	}

	private static boolean isTableExists(RuleClient tableDetailsClient,
                                         String tableNameTemp) throws SQLException {
        boolean isTableExists;
        Optimizer.setTableBeans(new TableInformationBeans());
        Optimizer.getTableBeans().setTableName(tableNameTemp);

		isTableExists = ((TableDetailsClient) tableDetailsClient)
				.fetchTableColumnDescription(tableNameTemp);
		return isTableExists;
	}

	private static void populateTableInfoBeans(RuleClient tableDetailsClient,
			String tableNameTemp) throws SQLException {
		((TableDetailsClient) tableDetailsClient)
				.fetchTableKeysDescription(tableNameTemp);

		((TableDetailsClient) tableDetailsClient)
				.fetchTablePartitions(tableNameTemp);

		((TableDetailsClient) tableDetailsClient)
				.fetchTableIndexes(tableNameTemp);

		if (null == Optimizer.getTableInformation()) {
			Optimizer.setTableInformation(new HashMap<>());
		}

		Map<String, List<String>> aliasDetailsTemp = SQLParserUtil
				.getAliasNameOfTables();

		if (null != aliasDetailsTemp) {
			Optimizer.getTableBeans().setAliasName(
					aliasDetailsTemp.get(tableNameTemp));
		}

		Optimizer.getTableInformation().put(
				Optimizer.getTableBeans().getTableName(),
				Optimizer.getTableBeans());
	}
}
