package com.util;

import com.rule.client.DataBaseClient;
import com.rule.client.RuleClient;
import com.rule.client.impl.DefaultDataBaseClient;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class represents a utility class to fetch the query plan on a specific
 * dataBase client for a specific query String passed as input parameter
 *
 */
public class QueryPlanUtil {

	/**
	 * The method is used to execute a query plan for a specific query,DB tuple.
     *
     * @param inputQuery
     * @param databaseClient
     * @return
     */
	public static String executeQueryPlan(String inputQuery,
                                          RuleClient databaseClient) throws SQLException {

        StringBuilder queryPlan = new StringBuilder();
        ResultSet result = null;
        Connection connection = null;
        DataBaseClient dbClient = (databaseClient instanceof DefaultDataBaseClient ? (DataBaseClient) databaseClient
                : null);

		try {

			// The inputQuery is all caps ON and is free from any sort of
			// comments at this point
            assert dbClient != null;
            connection = dbClient.getConnection();

			// Execute the query plan on
			result = dbClient.executeQueryPlan(inputQuery, connection);

			// Populate the predicates information
			populatePredicateInformation(result, queryPlan);

			// Check for hardParsing
            checkForHardParsing(queryPlan, inputQuery);
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
			System.out.println(queryPlan);
			if (null != databaseClient) {
                assert dbClient != null;
                assert result != null;
                dbClient.clearResource(result.getStatement(), result, connection);
            }
        }

		return queryPlan.toString();
	}

	/**
	 * Performs the query parsing to locate any hardParsing in it.
	 * TODO: Pankaj to explain the parsing logic here
	 * @param queryPlan
     * @param inputQuery
     */
    private static void checkForHardParsing(StringBuilder queryPlan, String inputQuery) {
        if (null == queryPlan) {

			int counterOpenBracket = 0;
			int counterClosedBracket = 0;
            int length = inputQuery.length();

			for (int i = 0; i < length; i++) {
				if (inputQuery.charAt(i) == '(') {
					counterOpenBracket++;
				}

				else if (inputQuery.charAt(i) == ')') {
					counterClosedBracket++;
				}
			}

			if (counterOpenBracket > counterClosedBracket) {
				queryPlan = new StringBuilder();
				queryPlan.append("Hard Parsing");

			} else {
				queryPlan = new StringBuilder();
				queryPlan
						.append("Either table does not exist or you don't have rights to get this information");
			}
		}
	}

	/**
	 * Populates the predicate information after parsing the resultSet. The
	 * information is populated in the @param queryPlan itself
	 * 
	 * @param result
	 * @param queryPlan
	 * @throws SQLException
	 */
	private static void populatePredicateInformation(ResultSet result,
			StringBuilder queryPlan) throws SQLException {
		if (null != result) {
			while (result.next()) {
				String currentRow = result.getString(1);

				if (currentRow.contains("Predicate Information")
						|| currentRow.equalsIgnoreCase("Note")) {
					break;
				}

                if (!currentRow.contains("Plan hash value")) {
                    assert queryPlan != null;
                    queryPlan.append(System.getProperty("line.separator"));
                    queryPlan.append(currentRow);
				}
			}
		}

	}
}
