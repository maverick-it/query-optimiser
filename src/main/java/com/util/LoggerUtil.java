package com.util;

import com.propertiesutil.UtilityClass;

import java.io.IOException;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerUtil {

    private static Logger logger = null;

    static {
        logger = Logger.getLogger(LoggerUtil.class.getName());
        try {

            Handler handler = new FileHandler("optimizer.log", 1048576, 10, true);
            Formatter formatter = new CustomLogFormatter();
            handler.setFormatter(formatter);
            logger.addHandler(handler);
        } catch (IOException e) {
            performLogging("static Block", e.getMessage(), "");
        }

        logger.setLevel(Level.ALL);
        logger.setUseParentHandlers(false);
    }

    public static void performLogging(String info) {

        logger.info(info);

    }

    public static void performLogging() {

        performLogging("");

    }

    public static void performLogging(String className, String methodName, String message) {
        performLogging(className, methodName, message, "");
    }

    private static void performLogging(String className, String methodName, String message,
                                       String additionalInfo) {
        logger.info("Class Name :" + className
                + " || Method Name :" + methodName + "()  || " + message
                + " || " + additionalInfo);

    }

    public static void logAnalysisDetails(long startTime, long endTime) {
        performLogging("**********Analysis Done ************");
        performLogging("Took " + ((endTime - startTime) / (1000 * 60))
                + " minutes " + (((endTime - startTime) / (1000)) % 60)
                + " seconds");
        performLogging("---------------------------------------------------------------------------------------");
        System.out.println("**********Analysis Done ************");
        System.out.println("Took " + ((endTime - startTime) / (1000 * 60))
                + " minutes " + (((endTime - startTime) / (1000)) % 60)
                + " seconds\n");
    }

    public static void reportInvalidQueryIssue(Map<String, Object> resultMap, Exception exception) {
        String violation;
        violation = UtilityClass.getMessage("msg.InvalidQuery", null);
        resultMap.put("msg.InvalidQuery", violation);
        performLogging(exception.getMessage());
    }
}

