package com.util;

import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.optimiser.Optimizer;
import com.parser.util.SQLParserUtil;
import com.propertiesutil.UtilityClass;
import com.rule.client.GeneralRuleClient;
import com.rule.client.IndexRuleClient;
import com.rule.client.PartitionRuleClient;
import com.rule.client.Rule;
import com.rule.client.RxConnectRuleClient;
import com.rule.client.impl.DefaultDataBaseClient;

import java.util.Map;

public class RuleExecutionUtil {

	private static final Rule rule = Optimizer.getRuleClient();
	private static Map<String, TableInformationBeans> tableInformation = Optimizer
			.getTableInformation();

	public static void executeRulesOnEachQuery(String query) throws Exception {

		Map<String, QueryBeans> fullTableScan = SQLParserUtil
				.getFullStatementDetails();

		GeneralRuleClient generalRuleClient = (GeneralRuleClient) rule
				.generalRuleClient();

        DefaultDataBaseClient defaultDataBase = (DefaultDataBaseClient) rule.databaseClient();
        boolean isValidQuery = defaultDataBase.executeQueryPlanOnMainQuery(
                fullTableScan, query);

		if (isValidQuery) {
			generalRuleClient.fetchQueryPlan(fullTableScan);
			PartitionRuleClient partitionRuleClient = (PartitionRuleClient) (rule
					.partitionRuleClient());
			partitionRuleClient.executePartitionRuleEngine(fullTableScan,
					Optimizer.getTableInformation(), query);
			if ("Y".equalsIgnoreCase(UtilityClass
					.getSetRules("TableNotPartitioned"))) {
				partitionRuleClient.checkIsTablePartitioned(fullTableScan,
						Optimizer.getTableInformation(), query);
			}

			generalRuleClient.executeGeneralRuleEngine(rule.getQueryText(),
					fullTableScan, Optimizer.getTableInformation());

			RxConnectRuleClient rxConnectRuleClient = (RxConnectRuleClient) (rule
					.rxConnectRuleClient());

			rxConnectRuleClient.executeRxConnectRules(fullTableScan,
					tableInformation);
			IndexRuleClient indexRuleClient = (IndexRuleClient) rule
					.indexRuleClient();

			indexRuleClient.executeIndexRuleEngine(fullTableScan,
					tableInformation, query);

			if ("Y".equalsIgnoreCase(UtilityClass
					.getSetRules("MergeCartesianRule"))) {

				generalRuleClient.executeMergerCartesianRule(fullTableScan,
						tableInformation, query);
			}
		}
	}
}
