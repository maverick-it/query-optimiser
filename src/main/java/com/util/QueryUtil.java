package com.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * The class provides various utility methods for the input query to be
 * processed by the optimization Engine
 *
 */
public class QueryUtil {

	/**
	 * The method is used to remove various types of comments from the query.
	 * The implementation is based on various patterns that are looked for inthe
	 * input query
	 * 
	 * @param _queryLocal
	 * @return
	 */
	public static String removeCommentsFromSqlQuery(String inputQuery) {

		Pattern commentPattern = Pattern.compile("/\\*.*?\\*/", Pattern.DOTALL);
		inputQuery = commentPattern.matcher(inputQuery).replaceAll("");
		
		commentPattern = Pattern.compile("--[^\n]*", Pattern.DOTALL);
		inputQuery = commentPattern.matcher(inputQuery).replaceAll("");
		inputQuery = inputQuery.replaceAll("\\s+", " ");
		
		
		//These partition based prefixes are hardCoded as of now
		// TODO : Update the client code to include checks around these primary key preFixes at client end 
		
		if (inputQuery.contains("_P#")) {
			inputQuery = inputQuery.replaceAll("_P#", "_P01");
		} else if (inputQuery.contains("_P$")) {
			inputQuery = inputQuery.replaceAll("_P\\$", "_P01");
		}
		
		if (inputQuery.contains("_#")) {
			inputQuery = inputQuery.replaceAll("_#", "_01");
		} else if (inputQuery.contains("_$")) {
			inputQuery = inputQuery.replaceAll("_\\$", "_01");
		}
		
		return inputQuery;
	}

	/**
	 * The method is used to read the query to be optimized. The query needs to
	 * be placed in a text file named as Demo.txt
	 * 
	 * The application will read the query from Demo.txt and will pass it down
	 * the line for further optimization steps
	 * 
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String readInput() throws Exception {
		InputStream inputStream = QueryUtil.class
				.getResourceAsStream("/Demo.txt");
		BufferedReader read = new BufferedReader(new InputStreamReader(
				inputStream));

		StringBuilder queryText = new StringBuilder();
		String text;

		// Read until EOF
		while (null != (text = read.readLine())) {
			queryText.append(text);
			queryText.append(System.getProperty("line.separator"));
		}

		return queryText.toString().toUpperCase();
	}
}
