package com.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.bean.OptimizeSuggestBean;
import com.bean.QueryBeans;
import com.bean.TableInformationBeans;
import com.optimiser.Optimizer;
import com.optimiser.SQLQueryOptimizer;
import com.parser.util.SQLParserUtil;
import com.propertiesutil.UtilityClass;

public class ViolationUtil {

	public static Map<String, Object> reportViolations(boolean isHardParsing) {

		Map<String, Object> result = new HashMap<String, Object>();

		LoggerUtil.performLogging("\n-----------------------------------------------------\n");

		List<String> violationList = null;
		StringBuilder violations = new StringBuilder();
		Map<String, QueryBeans> fullTableScan = SQLParserUtil.getFullStatementDetails();
		int counter = 0;
		boolean isInvalidQuery = true;
		if (null != fullTableScan) {
			isInvalidQuery = false;
			Iterator<Map.Entry<String, QueryBeans>> iterator = fullTableScan
					.entrySet().iterator();
			boolean isQueryOk = true;
			while (iterator.hasNext()) {

				Map.Entry<String, QueryBeans> entry = iterator.next();

				QueryBeans queryBean = entry.getValue();

				if (null != queryBean && null != queryBean.getViolationList()) {

					for (OptimizeSuggestBean violation : queryBean
							.getViolationList()) {

						String partition = "";
						isQueryOk = false;
						if (null != violation.getPartitionColumns()
								&& !violation.getPartitionColumns().isEmpty()) {

							for (String partitionTemp : violation
									.getPartitionColumns()) {

								partition = partition + " " + partitionTemp;
							}
						}

						String[] parameter = null;

						if (null != violation.getTableName()
								&& (!"".equalsIgnoreCase(partition) || null != violation
										.getColumnName())) {

							parameter = new String[2];
							parameter[0] = violation.getTableName();
							parameter[1] = !"".equalsIgnoreCase(partition) ? partition
									: violation.getColumnName();

						} else if (null != violation.getTableName()) {

							parameter = new String[2];
							parameter[0] = violation.getTableName();

						} else if (!"".equalsIgnoreCase(partition)) {

							parameter = new String[2];
							parameter[1] = partition;

						}

						if (null == violationList) {
							violationList = new ArrayList<>();
						}
						if (!violationList.contains(UtilityClass.getMessage(
								violation.getMessage(), parameter))) {

							violationList.add(UtilityClass.getMessage(
									violation.getMessage(), parameter));

							violations
									.append(counter++)
									.append(1)
									.append("")
									.append(": ")
									.append(UtilityClass.getMessage(
											violation.getMessage(), parameter))
									.append("\n");

							violation.setFinalMessage(UtilityClass.getMessage(
									violation.getMessage(), parameter));

							LoggerUtil.performLogging(counter
									+ ""
									+ ": "
									+ UtilityClass.getMessage(
											violation.getMessage(), parameter)
									+ "\n");

							System.out.println(counter
									+ ""
									+ ": "
									+ UtilityClass.getMessage(
											violation.getMessage(), parameter)
									+ "\n");
						}

					}
					result.put("QueryBeanMap", fullTableScan);
				}

				result.put("tableInformation",
						SQLQueryOptimizer.getTableInformation());
			}
			if (isQueryOk) {

				violations.append(UtilityClass.getMessage(
						"msg.AlreadyOptomized", null));

				LoggerUtil.performLogging(UtilityClass.getMessage("msg.AlreadyOptomized",
						null));
				System.out.println(UtilityClass.getMessage(
						"msg.AlreadyOptomized", null));

				result.put("msg.AlreadyOptomized",
						UtilityClass.getMessage("msg.AlreadyOptomized", null));
			}
		}
		if (isHardParsing) {
			violations.append(UtilityClass.getMessage("msg.HardParsing", null));
			LoggerUtil.performLogging(UtilityClass.getMessage("msg.HardParsing", null));
			System.out
					.println(UtilityClass.getMessage("msg.HardParsing", null));

			result.put("msg.HardParsing",
					UtilityClass.getMessage("msg.HardParsing", null));
			isInvalidQuery = false;

		}
		if (isInvalidQuery) {
			violations
					.append(UtilityClass.getMessage("msg.InvalidQuery", null));
			LoggerUtil.performLogging(UtilityClass.getMessage("msg.InvalidQuery", null));
			System.out.println(UtilityClass
					.getMessage("msg.InvalidQuery", null));

			result.put("msg.InvalidQuery",
					UtilityClass.getMessage("msg.InvalidQuery", null));
		}

		return result;
	}

	public static void reportTableNotExistsIssue() {
		TableInformationBeans tableInformationBeans = Optimizer.getTableBeans();
		OptimizeSuggestBean violation = new OptimizeSuggestBean();
		
		violation.setMessage("msg.TableNotExistInDatabase");
		violation.setTableName(tableInformationBeans.getTableName());
		
		Map<String, QueryBeans> fullTableScan = SQLParserUtil.getFullStatementDetails();
		
		for (Map.Entry<String, QueryBeans> entry : fullTableScan.entrySet()) {
			QueryBeans queryBean = entry.getValue();
			if (queryBean.getQuery().contains(
					tableInformationBeans.getTableName())) {
				queryBean.setViolation(violation);
			}
		}
	}
}
