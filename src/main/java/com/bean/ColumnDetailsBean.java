package com.bean;

public class ColumnDetailsBean {

	private String columnName;
	private String operator;
	private String columnValue;

	public String getColumnName() {
		return columnName;
	}

    public void setColumnName(String _columnNameLocal) {
        columnName = _columnNameLocal;
    }

	public String getOperator() {
		return operator;
	}

	public void setOperator(String _operatorLocal) {
		operator = _operatorLocal;
    }

    public String getColumnValue() {
        return columnValue;
    }

	public void setColumnValue(String _columnValueLocal) {
		columnValue = _columnValueLocal;
	}

}
