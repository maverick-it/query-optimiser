package com.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QueryBeans {
	
	private String queryId; 
	
	private String query;

	private List<ColumnDetailsBean> whereClauseColumnList;

    private List<ColumnDetailsBean> selectColumnName;

	private Map<String, String> aliasMap;

    private List<OptimizeSuggestBean> violationList;

	private List<Map<String, Map<String, String>>> explainPlan;
    private String statementType;

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

	public List<ColumnDetailsBean> getWhereClauseColumnList() {
		return whereClauseColumnList;
	}

	public void setWhereClauseColumnList(
			List<ColumnDetailsBean> whereClauseColumnList) {
		this.whereClauseColumnList = whereClauseColumnList;
	}

    public List<ColumnDetailsBean> getSelectColumnName() {
        return selectColumnName;
    }

    public void setSelectColumnName(List<ColumnDetailsBean> selectColumnName) {
        this.selectColumnName = selectColumnName;
    }

	public Map<String, String> getAliasMap() {
		return aliasMap;
	}

	public void setAliasMap(Map<String, String> aliasMap) {
		this.aliasMap = aliasMap;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setViolation(OptimizeSuggestBean _violationLocal) {

        if (null == violationList) {

            violationList = new ArrayList<>();
        }

        violationList.add(_violationLocal);
    }

    public List<OptimizeSuggestBean> getViolationList() {
        return violationList;
    }

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String _queryIdLocal) {
		queryId = _queryIdLocal;
	}

	public void setExplainPlanDetails(
			List<Map<String, Map<String, String>>> _listOfOperationsLocal) {

		explainPlan = _listOfOperationsLocal;
	}

	public List<Map<String, Map<String, String>>> getExplainPlan() {
		return explainPlan;
    }

    public void setExplainPlan(List<Map<String, Map<String, String>>> explainPlan) {
        this.explainPlan = explainPlan;
    }
}
