package com.bean;

import com.propertiesutil.UtilityClass;

import java.util.List;


public class OptimizeSuggestBean {

	private String message;

	private String tableName;

	private String indexName;

    private List<String> partitionColumns;

	private String ColumnName ;
	
	private String rulePriority;
	
	private String finalMessage;

    public List<String> getPartitionColumns() {
        return partitionColumns;
    }

    public void setPartitionColumns(List<String> _partitionInfoLocal) {

        partitionColumns = _partitionInfoLocal;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String _messageLocal) {
		message = _messageLocal;
		
		if (null != UtilityClass.getSeverity(UtilityClass
				.getMapping(_messageLocal))) {
			setRulePriority(UtilityClass.getSeverity(UtilityClass
					.getMapping(_messageLocal)));
		} else {
			setRulePriority("Critical");
		}
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String _tableNameLocal) {
		tableName = _tableNameLocal;
    }

    public String getIndexName() {
        return indexName;
    }

	public void setIndexName(String _indexNameLocal) {
		indexName = _indexNameLocal;
	}

	public String getColumnName() {
		return ColumnName;
	}

	public void setColumnName(String _columnNameLocal) {
		ColumnName = _columnNameLocal;
	}

	public String getRulePriority() {
		return rulePriority;
	}

	private  void setRulePriority(String _rulePriorityLocal) {
		rulePriority = _rulePriorityLocal;
	}

	public String getFinalMessage() {
		return finalMessage;
	}

	public void setFinalMessage(String _finalMessageLocal) {
		finalMessage = _finalMessageLocal;
	}

}
