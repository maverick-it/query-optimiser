package com.bean;

import java.util.List;
import java.util.Map;

public class TableInformationBeans {
	private String tableName;
	private List<String> aliasName;
	private List<ColumnDetailsBean> whereClauseDetails;
	private List<String> primaryKeysDescription;
	private Map<String, List<Object>> tableColumnDescription;
	private List<Map<String,String>> foreignKeysDescription;
	private Map<String, List<String>> tableIndexesDescription;
    private List<String> partitionKeyList;

    public List<String> getAliasName() {
        return aliasName;
	}
	public void setAliasName(List<String> aliasName) {
		this.aliasName = aliasName;
	}
	public List<ColumnDetailsBean> getWhereClausedetails() {
		return whereClauseDetails;
	}
	public void setWhereClausedetails(List<ColumnDetailsBean> columndetails) {
		this.whereClauseDetails = columndetails;
	}
	public List<Map<String,String>> getForeignKeysDescription() {
		return foreignKeysDescription;
	}
	public void setForeignKeysDescription(List<Map<String,String>> foreignKeysDescription) {
		this.foreignKeysDescription = foreignKeysDescription;
	}

    public List<String> getPartitionKeyList() {
        return partitionKeyList;
    }

    public void setPartitionKeyList(List<String> partitionKeyList) {
        this.partitionKeyList = partitionKeyList;
    }

    public List<String> getPrimaryKeysDescription() {
		return primaryKeysDescription;
	}
	public void setPrimaryKeysDescription(List<String> primaryKeysDescription) {
		this.primaryKeysDescription = primaryKeysDescription;
	}
	public Map<String, List<String>> getTableIndexesDescription() {
		return tableIndexesDescription;
	}
	public void setTableIndexesDescription(
			Map<String, List<String>> tableIndexesDescription) {
		this.tableIndexesDescription = tableIndexesDescription;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public Map<String, List<Object>> getTableColumnDescription() {
		return tableColumnDescription;
	}
	public void setTableColumnDescription(
			Map<String, List<Object>> tableColumnDescription) {
		this.tableColumnDescription = tableColumnDescription;
	}

}
