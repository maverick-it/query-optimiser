package com.propertiesutil;

public class ClientConverter<Q, R> {

	/**
	 * @param objectToBeConverted
	 * @param destinationType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public R convert(Q objectToBeConverted, Class<R> destinationType) {
		return (R) objectToBeConverted;
	}
}
