package com.propertiesutil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import static com.util.LoggerUtil.performLogging;

public class UtilityClass
{

  private static PropertiesUtil properties = new PropertiesUtil();

  private static PropertiesUtil message = new PropertiesUtil();

  private static PropertiesUtil rules = new PropertiesUtil();

  private static PropertiesUtil setRules = new PropertiesUtil();

  private static PropertiesUtil login = new PropertiesUtil();

    private static PropertiesUtil severity = new PropertiesUtil();

    private static PropertiesUtil mapping = new PropertiesUtil();

  static
  {
  Map<String,PropertiesUtil> filesTOLoad = new HashMap<>();
    filesTOLoad.put("configuration.properties", properties);
    filesTOLoad.put("message.properties", message);
    filesTOLoad.put("rule.properties", rules);
    filesTOLoad.put("setRule.properties", setRules);
    filesTOLoad.put("login.properties", login);
    filesTOLoad.put("severity.properties", severity);
    filesTOLoad.put("mapping.properties", mapping);
      Set<String> fileset = filesTOLoad.keySet();

      for (String file : fileset)
    {
        InputStream inputStream = null;

        try
      {
          inputStream = UtilityClass.class.getResourceAsStream("/config/" + file);
          PropertiesUtil tempProperties = filesTOLoad.get(file);
          tempProperties.load(inputStream);
      }
      catch (IOException e) {
          performLogging(e.getMessage());
        e.printStackTrace();
        try
        {
          inputStream.close();
        }
        catch (IOException ex) {
            performLogging(e.getMessage());
          ex.printStackTrace();
        }
      }
      finally
      {
        try
        {
            assert inputStream != null;
            inputStream.close();
        }
        catch (IOException e) {
            performLogging(e.getMessage());
          e.printStackTrace();
        }
      }
    }
  }

  public static String getProperties(String _stringLocal)
  {
      return properties.get(_stringLocal);
  }

    public static String getMessage(String _stringLocal, String[] args)
  {
      String messageTemp = message.get(_stringLocal);

    if (args != null) {
      MessageFormat format = new MessageFormat(messageTemp);
      messageTemp = format.format(args);
    }
    return messageTemp;
  }

  public static String getRule(String _stringLocal)
  {
      return rules.get(_stringLocal);
  }

  public static String getSetRules(String _stringLocal)
  {
      return setRules.get(_stringLocal);
  }

  public static String getSeverity(String _local){

      return severity.get(_local);
  }
  
  public static String getMapping(String _local){

      return mapping.get(_local);
  }


    public static PropertiesUtil getDefaultRules()
  {
    String file = "setRule.properties";

    PropertiesUtil ruleProperties = null;
    File directory = new File("");
    String currentDirectoryPath = directory.getAbsolutePath();

    File configFolder = new File(currentDirectoryPath);
    String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/" + 
      file;

    InputStream inputStream = null;
    try {
      inputStream = new FileInputStream(propertiesFile);
    }
    catch (FileNotFoundException e1) {
        performLogging(e1.getMessage());
      e1.printStackTrace();
    }
    try
    {
      ruleProperties = new PropertiesUtil();

      ruleProperties.load(inputStream);
    }
    catch (IOException e) {
        performLogging(e.getMessage());
      e.printStackTrace();
      try
      {
          assert inputStream != null;
          inputStream.close();
      }
      catch (IOException ex) {
          performLogging(e.getMessage());
        ex.printStackTrace();
      }
    }
    finally
    {
      try
      {
          assert inputStream != null;
          inputStream.close();
      }
      catch (IOException e) {
          performLogging(e.getMessage());
        e.printStackTrace();
      }
    }

    return ruleProperties;
  }


    public static PropertiesUtil getDefaultDbDetails()
  {
    String file = "configuration.properties";

    PropertiesUtil dbProperties = null;

    File directory = new File("");
    String currentDirectoryPath = directory.getAbsolutePath();

    File configFolder = new File(currentDirectoryPath);
    String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/" + 
      file;

    InputStream inputStream = null;
    try {
      inputStream = new FileInputStream(propertiesFile);
    }
    catch (FileNotFoundException e1) {
        performLogging(e1.getMessage());
      e1.printStackTrace();
    }
    try
    {
      dbProperties = new PropertiesUtil();

      dbProperties.load(inputStream);
    }
    catch (IOException e) {
        performLogging(e.getMessage());
      e.printStackTrace();
      try
      {
          assert inputStream != null;
          inputStream.close();
      }
      catch (IOException ex) {
          performLogging(e.getMessage());
        ex.printStackTrace();
      }
    }
    finally
    {
      try
      {
          assert inputStream != null;
          inputStream.close();
      }
      catch (IOException e) {
          performLogging(e.getMessage());
        e.printStackTrace();
      }
    }

    return dbProperties;
  }


    public static PropertiesUtil getDefaultColumnRuleList() {
        String file = "rule.properties";

		PropertiesUtil columnRuleProperties = null;

		File directory = new File("");
		String currentDirectoryPath = directory.getAbsolutePath();

		File configFolder = new File(currentDirectoryPath);
		String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/"
				+ file;

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(propertiesFile);
		} catch (FileNotFoundException e1) {
            performLogging(e1.getMessage());
            e1.printStackTrace();
        }
        try {
			columnRuleProperties = new PropertiesUtil();

			columnRuleProperties.load(inputStream);
		} catch (IOException e) {
            performLogging(e.getMessage());
            e.printStackTrace();
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException ex) {
                performLogging(e.getMessage());
                ex.printStackTrace();
            }
        } finally {
			try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                performLogging(e.getMessage());
                e.printStackTrace();
            }
        }

		return columnRuleProperties;
	}

  public static String getLogin(String _stringLocal)
  {
      return login.get(_stringLocal);
  }

    public static void updateRulesInPropertyfile(Map<String, String> updatedSetOfRules)
  {
    File directory = new File("");
    String currentDirectoryPath = directory.getAbsolutePath();

    File configFolder = new File(currentDirectoryPath);
    String propertiesFile = configFolder.getAbsolutePath() + 
      "/temp/config/" + "setRule.properties";

      FileOutputStream fileOut;
    try {
      fileOut = new FileOutputStream(propertiesFile);
      Properties properties = new Properties();
   Set<Entry<String, String>> entrySet = updatedSetOfRules.entrySet();
      for (Entry<String, String> eSet : entrySet)
      {
          properties.setProperty(eSet.getKey(), eSet.getValue());
          setRules.setProperty(eSet.getKey(), eSet.getValue());
      }
      properties.store(fileOut, "Rules Set");
      fileOut.close();
    } catch (IOException e1) {
        performLogging(e1.getMessage());
      e1.printStackTrace();
    }
  }

    public static void updateDBDetailsInPropertyfile(Map<String, String> updatedDBDetails)
  {
    File directory = new File("");
    String currentDirectoryPath = directory.getAbsolutePath();

    File configFolder = new File(currentDirectoryPath);
    String propertiesFile = configFolder.getAbsolutePath() + 
      "/temp/config/" + "configuration.properties";

      FileOutputStream fileOut;
    try {
      fileOut = new FileOutputStream(propertiesFile);
      Properties propertiesLocal = new Properties();
      Set<Entry<String, String>> entrySet = updatedDBDetails.entrySet();
      for (Entry<String, String> eSet : entrySet)
      {
          propertiesLocal.setProperty(eSet.getKey(), eSet.getValue());
          properties.setProperty(eSet.getKey(), eSet.getValue());
      }
      propertiesLocal.store(fileOut, "Database Details");
      fileOut.close();
    } catch (IOException e1) {
        performLogging(e1.getMessage());
      e1.printStackTrace();
    }
  }

	public static void updateDefaultColumnRuleProperties(
            Map<String, String> updatedColumnDetails) {
        File directory = new File("");
        String currentDirectoryPath = directory.getAbsolutePath();

		File configFolder = new File(currentDirectoryPath);
		String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/"
				+ "rule.properties";

        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(propertiesFile);
			Properties propertiesLocal = new Properties();
			Set<Entry<String, String>> entrySet = updatedColumnDetails
					.entrySet();
			for (Entry<String, String> eSet : entrySet) {
                propertiesLocal.setProperty(eSet.getKey(),
                        eSet.getValue());
                rules.setProperty(eSet.getKey(),
                        eSet.getValue());
            }
            propertiesLocal.store(fileOut, "Column Rule Set");
            fileOut.close();
		} catch (IOException e1) {
            performLogging(e1.getMessage());
            e1.printStackTrace();
        }
    }
	

	/**
	 * This method is used to read severity property file and fetch all details to
	 * put in a  map with Rule as key and Severity as value.
	 * @return Map: Contains map with Rule as key and Severity as value
	 */

    public static Map<String, String> getSeverityOfRules() {
        String file = "severity.properties";

		PropertiesUtil severityProperties = null;

		File directory = new File("");
		String currentDirectoryPath = directory.getAbsolutePath();

		File configFolder = new File(currentDirectoryPath);
		String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/"
				+ file;

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(propertiesFile);
		} catch (FileNotFoundException e1) {
            performLogging(e1.getMessage());
            e1.printStackTrace();
        }
        try {
			severityProperties = new PropertiesUtil();

			severityProperties.load(inputStream);
		} catch (IOException e) {
            performLogging(e.getMessage());
            e.printStackTrace();
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException ex) {
                performLogging(e.getMessage());
                ex.printStackTrace();
            }
        } finally {
			try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                performLogging(e.getMessage());
                e.printStackTrace();
            }
        }

		return severityProperties;
	}
	
	public static void updateDefaultSeveritySetOfRules(
            Map<String, String> updatedSeveritysSetOfRules) {
        File directory = new File("");
        String currentDirectoryPath = directory.getAbsolutePath();

        File configFolder = new File(currentDirectoryPath);
        String propertiesFile = configFolder.getAbsolutePath() + "/temp/config/"
                + "severity.properties";

        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream(propertiesFile);
            Properties propertiesLocal = new Properties();
            Set<Entry<String, String>> entrySet = updatedSeveritysSetOfRules
                    .entrySet();
            for (Entry<String, String> eSet : entrySet) {
                propertiesLocal.setProperty(eSet.getKey(),
                        eSet.getValue());
                severity.setProperty(eSet.getKey(),
                        eSet.getValue());
            }
            propertiesLocal.store(fileOut, "Severity Of Rules");
            fileOut.close();
        } catch (IOException e1) {
            performLogging(e1.getMessage());
            e1.printStackTrace();
        }
    }
}