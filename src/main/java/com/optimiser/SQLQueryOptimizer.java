package com.optimiser;

import java.util.Map;

import com.rule.client.HardParseRuleClient;
import com.rule.client.RuleClient;
import com.rule.client.RuleFactory;
import com.rule.client.impl.DefaultTableDetailsClient;
import com.util.BeanInitializationUtil;
import com.util.LoggerUtil;
import com.util.QueryPlanUtil;
import com.util.QueryUtil;
import com.util.RuleExecutionUtil;
import com.util.ViolationUtil;

/**
 * The main class that initiates the flow of query optimization.FOllowing steps
 * are followed to streamLine the process of query optimization:
 * <ol>
 * <li>Get the query
 * <li>Remove Comments
 * <li>Get the corresponding Rule Client
 * <li>Parse the query to fetch the information
 * <li>Get the DataBase Client
 * <li>Fetch the Schema Info
 * <li>Execute Query Plan
 * <li>Execute Rules on query
 * <li>Report Violations
 * <li>Clean Up the resources
 * </ol>
 *
 * The application works for Oracle DataBase as of now but it will be extended
 * for other DBs soon.
 *
 */
public class SQLQueryOptimizer extends Optimizer {

	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();

		Map<String, Object> resultMap = null;
		try {

			// 1. Read the input query
			String query = QueryUtil.readInput();

			if (null == query || query.trim().length() == 0) {
				throw new IllegalArgumentException(
						"Please enter a valid query input");
			}

			// 2. Remove the comments from the query
			query = QueryUtil.removeCommentsFromSqlQuery(query);

			// 3. Fetch the Rule Client
			rules = RuleFactory.create(query);

			// 4. Parse the query to fetch the information
			RuleClient tableDetailsClient = rules.tableDetailsClient();

			// Get the table names
			tableNamesList = ((DefaultTableDetailsClient) tableDetailsClient)
					.fetchTableNames();

			// 5. Get the database client and fetch the DB schema info
			RuleClient databaseClient = rules.databaseClient();

			// 6. Fetch the query plan
			String fetchPlan = QueryPlanUtil.executeQueryPlan(query,
					databaseClient);

			//7. Execute the hardParsing step
			Map<String, String> result = ((HardParseRuleClient) rules
					.hardParseRuleClient()).executeHardParseRule(fetchPlan);

			//8. Initialize the table information
			boolean isTableExists = BeanInitializationUtil
					.initializeTableInformation(tableDetailsClient);

			//9. execute the rules
			if (continueExecution(isTableExists)) {
				RuleExecutionUtil.executeRulesOnEachQuery(query);
			}

			//10. Populate violations
			resultMap = ViolationUtil.reportViolations(Boolean.valueOf(result
					.get("isHardParsing")));
			
			resultMap.put("queryPlan", result.get("fetchPlan"));

		} catch (Exception exception) {
			LoggerUtil.reportInvalidQueryIssue(resultMap, exception);
		} finally {
			cleanUpResource();
		}
		
		
		long endTime = System.currentTimeMillis();
		LoggerUtil.logAnalysisDetails(startTime, endTime);
	}
}
