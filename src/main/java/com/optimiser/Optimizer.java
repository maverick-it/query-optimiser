package com.optimiser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bean.TableInformationBeans;
import com.parser.util.SQLParserUtil;
import com.rule.client.Rule;

/**
 * The abstract class that acts as the top level class in the Optimizer
 * hierarchy. Most of the member variables are initialized in the concrete
 * Implementation of this abstract class i.e {@link SQLQueryOptimizer}
 */
public abstract class Optimizer {

	//A list of the table names as mentioned in the input query
	protected static List<String> tableNamesList = null;
	
	//A container that hosts different types of rule client for this query
	protected static Rule rules = null;

	//Table and its column's mapping
	private static Map<String, Map<String, List<Object>>> tableColumnDescription = null;

	//Table Key's mapping
	private static Map<String, Map<Integer, List<String>>> tableKeysDescription = null;

	//Table Index's mapping
	private static Map<String, Map<String, List<String>>> tableIndexesDescription = null;

	//Table Partition's mapping
	private static Map<String, List<String>> tablePartitionDescription = null;
	
	//Permissible Operations
	private static Map<String, Map<String, String>> operationMap = null;
	
	private static List<String> listOfHeadings = new ArrayList<>();

	private static List<Map<String, Map<String, String>>> listOfOperations = new ArrayList<>();

	private static List<String> accessOperations = new ArrayList<>();
	private static List<String> filterOperations = new ArrayList<>();
	private static Map<String, TableInformationBeans> tableInformation = null;
	private static TableInformationBeans tableBeans = null;

	public static Map<String, Map<String, List<Object>>> getTableColumnDescription() {
		return tableColumnDescription;
	}

	public static void setTableColumnDescription(
			Map<String, Map<String, List<Object>>> _tableColumnDescriptionLocal) {
		tableColumnDescription = _tableColumnDescriptionLocal;
	}

	public static Rule getRuleClient() {
		return rules;
	}

	public static TableInformationBeans getTableBeans() {
		return tableBeans;
	}

	public static void setTableBeans(TableInformationBeans _tableBeansLocal) {
		tableBeans = _tableBeansLocal;
	}

	public static Map<String, Map<Integer, List<String>>> getTableKeysDescription() {
		return tableKeysDescription;
	}

	public static void setTableKeysDescription(
			Map<String, Map<Integer, List<String>>> _tableKeysDescriptionLocal) {
		tableKeysDescription = _tableKeysDescriptionLocal;
	}

	public static Map<String, List<String>> getTablePartitionDescription() {
		return tablePartitionDescription;
	}

	public static void setTablePartitionDescription(
			Map<String, List<String>> _tablePartitionDescriptionLocal) {
		tablePartitionDescription = _tablePartitionDescriptionLocal;
	}

	public static Map<String, Map<String, List<String>>> getTableIndexesDescription() {
		return tableIndexesDescription;
	}

	public static void setTableIndexesDescription(
			Map<String, Map<String, List<String>>> _tableIndexesDescriptionLocal) {
		tableIndexesDescription = _tableIndexesDescriptionLocal;
	}

	public static Map<String, Map<String, String>> getOperationMap() {
		return operationMap;
	}

	public static void setOperationMap(
			Map<String, Map<String, String>> _operationMapLocal) {
		operationMap = _operationMapLocal;
	}

	public static List<String> getListOfHeadings() {
		return listOfHeadings;
	}

	public static void setListOfHeadings(List<String> _listOfHeadingsLocal) {
		listOfHeadings = _listOfHeadingsLocal;
	}

	public static List<Map<String, Map<String, String>>> getListOfOperations() {
		return listOfOperations;
	}

	public static void setListOfOperations(
			List<Map<String, Map<String, String>>> _listOfOperationsLocal) {
		listOfOperations = _listOfOperationsLocal;
	}

	public static List<String> getAccessOperations() {
		return accessOperations;
	}

	public static List<String> getFilterOperations() {
		return filterOperations;
	}

	public static Map<String, TableInformationBeans> getTableInformation() {
		return tableInformation;
	}

	public static void setTableInformation(
			Map<String, TableInformationBeans> _tableInformationLocal) {
		tableInformation = _tableInformationLocal;
	}

	public static List<String> getTableNamesList() {
		return tableNamesList;
	}

	static void cleanUpResource() {
		tableColumnDescription = null;
		tableKeysDescription = null;
		tableIndexesDescription = null;
		tablePartitionDescription = null;
		operationMap = null;
		tableBeans = null;
		listOfHeadings = new ArrayList<>();
		listOfOperations = new ArrayList<>();
		accessOperations = new ArrayList<>();
		filterOperations = new ArrayList<>();
		tableInformation = null;
		tableNamesList = null;
		SQLParserUtil.setFullStatementDetails(null);
		SQLParserUtil.setAliasNameOfTables(null);
		SQLParserUtil.setTableNames(null);
	}

	static boolean continueExecution(boolean isTableExists) {
		return null != tableNamesList && !tableNamesList.isEmpty()
				&& isTableExists;
	}
}
