package com.parser.util;

import static com.util.LoggerUtil.performLogging;
import gudusoft.gsqlparser.EDbVendor;
import gudusoft.gsqlparser.EExpressionType;
import gudusoft.gsqlparser.EFunctionType;
import gudusoft.gsqlparser.TCustomSqlStatement;
import gudusoft.gsqlparser.TGSqlParser;
import gudusoft.gsqlparser.nodes.IExpressionVisitor;
import gudusoft.gsqlparser.nodes.TCaseExpression;
import gudusoft.gsqlparser.nodes.TExpression;
import gudusoft.gsqlparser.nodes.TFunctionCall;
import gudusoft.gsqlparser.nodes.TGroupByItem;
import gudusoft.gsqlparser.nodes.TInExpr;
import gudusoft.gsqlparser.nodes.TInsertCondition;
import gudusoft.gsqlparser.nodes.TInsertIntoValue;
import gudusoft.gsqlparser.nodes.TJoin;
import gudusoft.gsqlparser.nodes.TJoinItem;
import gudusoft.gsqlparser.nodes.TJoinItemList;
import gudusoft.gsqlparser.nodes.TMergeWhenClause;
import gudusoft.gsqlparser.nodes.TMultiTarget;
import gudusoft.gsqlparser.nodes.TOrderByItem;
import gudusoft.gsqlparser.nodes.TOrderByItemList;
import gudusoft.gsqlparser.nodes.TParseTreeNode;
import gudusoft.gsqlparser.nodes.TParseTreeNodeList;
import gudusoft.gsqlparser.nodes.TResultColumn;
import gudusoft.gsqlparser.nodes.TTable;
import gudusoft.gsqlparser.nodes.TTrimArgument;
import gudusoft.gsqlparser.nodes.TWhenClauseItem;
import gudusoft.gsqlparser.nodes.TWhenClauseItemList;
import gudusoft.gsqlparser.stmt.TAlterTableStatement;
import gudusoft.gsqlparser.stmt.TDeleteSqlStatement;
import gudusoft.gsqlparser.stmt.TDropTableSqlStatement;
import gudusoft.gsqlparser.stmt.TInsertSqlStatement;
import gudusoft.gsqlparser.stmt.TMergeSqlStatement;
import gudusoft.gsqlparser.stmt.TSelectSqlStatement;
import gudusoft.gsqlparser.stmt.TUpdateSqlStatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.bean.ColumnDetailsBean;
import com.bean.QueryBeans;

/**
 * This is Utility Class to Parse SQL Statements
 * 
 * @author 388524
 * 
 */
public class SQLParserUtil {

    private static final List<String> ignoreQueryList = new ArrayList<>();

    static {
        ignoreQueryList
                .add("SELECT CAST(COMMON_PKG.F_IN_LIST('A') AS IN_LIST_TYPE_1_6)FROM DUAL");
        ignoreQueryList
                .add("((SELECT * FROM THE (SELECT CAST(COMMON_PKG.F_IN_LIST(?) AS IN_LIST_TYPE_1_6) FROM DUAL)))");
        ignoreQueryList
                .add("(SELECT * FROM THE (SELECT CAST(COMMON_PKG.F_IN_LIST(?) AS IN_LIST_TYPE_1_6) FROM DUAL D) D WHERE ROWNUM >= 0)");
        ignoreQueryList
                .add("(SELECT * FROM THE (SELECT CAST(COMMON_PKG.F_IN_LIST(?) AS IN_LIST_TYPE_1_6)FROM DUAL))");
    }

    /**
     * Contains List of Aliases for a Table in Query Key of this map is table
     * name
	 */

    private static Map<String, List<String>> aliasNameOfTables = null;
    private static int queryId;
    /**
     * Contains List of all table names used in the Query
     */

    private static List<String> tableNames = null;
    /**
     * contains all the details related to a Query in a Bean Key of this map is
     * Query itself.
     */

    private static Map<String, QueryBeans> fullStatementDetails = null;
    /**
     * Private instance of TGSqlParser
	 */

    private static TGSqlParser sparser = new TGSqlParser(EDbVendor.dbvoracle);


    public SQLParserUtil(String sql) {
        sparser.sqltext = sql;
        int ret = sparser.parse();
        if (ret != 0) {
            System.err.println(sparser.getErrormessage());
        } else {
            for (int k = 0; k < sparser.sqlstatements.size(); k++) {
                if (sparser.sqlstatements.get(k) != null) {
                    analyzeStatement(sparser.sqlstatements
                            .get(k));
                }
            }
        }
	}


    public static List<String> getTableNames(String sqlFile) {

        tableNames = new ArrayList<>();
        EDbVendor dbVendor = EDbVendor.dbvoracle;

        performLogging("Selected SQL dialect: " + dbVendor.toString());

        new SQLParserUtil(sqlFile);

        return tableNames;
    }

    public static void getColumnsInWhereClause(String sqlText) {

        sparser.sqltext = sqlText;
        int i = sparser.parse();
        if (i == 0) {
            WhereCondition w = new WhereCondition(sparser.sqlstatements
                    .get(0).getWhereClause().getCondition());
            w.printColumn();
        } else
            performLogging(sparser.getErrormessage());
    }


    public static Map<String, List<String>> getAliasNameOfTables() {
        return aliasNameOfTables;
    }

    public static void setAliasNameOfTables(
            Map<String, List<String>> aliasNameOfTables) {
        SQLParserUtil.aliasNameOfTables = aliasNameOfTables;
    }


    public static Map<String, QueryBeans> getFullStatementDetails() {
        return fullStatementDetails;
    }

    public static void setFullStatementDetails(
            Map<String, QueryBeans> fullStatementDetails) {
        SQLParserUtil.fullStatementDetails = fullStatementDetails;
    }

    public static void setTableNames(List<String> tableNames) {
        SQLParserUtil.tableNames = tableNames;
    }

    public void analyzeStatement(TCustomSqlStatement stmt) {
		if (stmt instanceof TSelectSqlStatement) {
			Set<String> tables = analyzeSelectStatement((TSelectSqlStatement) stmt);
			if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    tableNames.add(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        } else if (stmt instanceof TInsertSqlStatement) {
            TInsertSqlStatement insert = (TInsertSqlStatement) stmt;
            Set<String> tables = analyzeInsertStatement(insert);
            if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    tableNames.add(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        } else if (stmt instanceof TUpdateSqlStatement) {
			TUpdateSqlStatement update = (TUpdateSqlStatement) stmt;
			Set<String> tables = analyzeUpdateStatement(update);
			if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    tableNames.add(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        } else if (stmt instanceof TDeleteSqlStatement) {
			TDeleteSqlStatement delete = (TDeleteSqlStatement) stmt;
			Set<String> tables = analyzeDeleteStatement(delete);
			if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    tableNames.add(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        } else if (stmt instanceof TDropTableSqlStatement) {
			TDropTableSqlStatement drop = (TDropTableSqlStatement) stmt;
            if (drop.getTableName() != null) {
                performLogging("Target: " + drop.getTableName().toString());
            }
        } else if (stmt instanceof TAlterTableStatement) {
            TAlterTableStatement alter = (TAlterTableStatement) stmt;
			if (alter.getTableName() != null) {
				System.out
				.println("Target: " + alter.getTableName().toString());
			}
		} else if (stmt instanceof TMergeSqlStatement) {
			TMergeSqlStatement merge = (TMergeSqlStatement) stmt;
			Set<String> tables = analyzeMergeStatement(merge);
			if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    tableNames.add(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        }

        performLogging();
    }


    private Set<String> analyzeMergeStatement(TMergeSqlStatement merge) {
        if (merge.getTargetTable() != null) {
            performLogging("Target: "
                    + merge.getTargetTable().getFullName());
        }
        Set<String> tables = new LinkedHashSet<>();
        if (merge.getUsingTable() != null) {
            if (merge.getUsingTable().isBaseTable())
				tables.add(merge.getUsingTable().getFullName());
			else if (merge.getUsingTable().getSubquery() != null) {
				tables.addAll(analyzeSelectStatement(merge.getUsingTable()
						.getSubquery()));
			}
			if (merge.getCondition() != null) {
				new tablesInExpr(this, merge.getCondition(), tables)
						.searchTable();
			}
			if (merge.getWhenClauses() != null) {
				for (int i = 0; i < merge.getWhenClauses().size(); i++) {
					TMergeWhenClause when = merge.getWhenClauses()
							.getElement(i);
					if (when.getCondition() != null) {
						new tablesInExpr(this, when.getCondition(), tables)
								.searchTable();
					}
					if (when.getInsertClause() != null
							&& when.getInsertClause().getValuelist() != null) {
						for (int j = 0; j < when.getInsertClause()
								.getValuelist().size(); j++) {
							TResultColumn field = when.getInsertClause()
									.getValuelist().getResultColumn(j);
							if (field.getExpr().getExpressionType() == EExpressionType.subquery_t) {
								tables.addAll(analyzeSelectStatement(field
										.getExpr().getSubQuery()));
							}
						}
					}
					if (when.getUpdateClause() != null) {
						if (when.getUpdateClause().getUpdateWhereClause() != null) {
							new tablesInExpr(this, when.getUpdateClause()
									.getUpdateWhereClause(), tables)
									.searchTable();
						}
						if (when.getUpdateClause().getUpdateColumnList() != null) {
							for (int j = 0; j < when.getUpdateClause()
									.getUpdateColumnList().size(); j++) {
								TResultColumn field = when.getUpdateClause()
										.getUpdateColumnList()
										.getResultColumn(j);
								if (field.getExpr().getRightOperand()
										.getExpressionType() == EExpressionType.subquery_t) {
									tables.addAll(analyzeSelectStatement(field
											.getExpr().getRightOperand()
											.getSubQuery()));
								}
                            }
                        }
                    }
                }
            }
            if (!tables.isEmpty()) {
                performLogging("Source: ");
                String[] tableArray = tables.toArray(new String[tables.size()]);
                for (int i = 0; i < tableArray.length; i++) {
                    performLogging(tableArray[i]);
                    if (i < tableArray.length - 1) {
                        performLogging(", ");
                    }
                }
                performLogging();
            }
        }
        if (null == fullStatementDetails) {
            fullStatementDetails = new HashMap<>();
        }

        QueryBeans queryDescription = new QueryBeans();

		queryDescription.setQueryId(++queryId + "");
		queryDescription.setQuery(merge.toString());
        queryDescription.setStatementType("MERGE");
        fullStatementDetails.put(queryDescription.getQuery(), queryDescription);
        return tables;
    }


    private Set<String> analyzeDeleteStatement(TDeleteSqlStatement delete) {
        Set<String> tables = new LinkedHashSet<>();

        if (delete.getTargetTable() != null) {
            performLogging("Target: "
                    + delete.getTargetTable().getFullName());
            tables.add(delete.getTargetTable().getFullName());
        }
        if (null == fullStatementDetails) {
            fullStatementDetails = new HashMap<>();
        }

        QueryBeans queryDescription = new QueryBeans();

		queryDescription.setQueryId(++queryId + "");
		queryDescription.setQuery(delete.toString());
        queryDescription.setStatementType("DELETE");
        if (delete.getResultColumnList() != null) {
            for (int i = 0; i < delete.getResultColumnList().size(); i++) {
				TResultColumn field = delete.getResultColumnList()
				.getResultColumn(i);
				if (field.getExpr().getRightOperand() != null
						&& field.getExpr().getRightOperand()
						.getExpressionType() == EExpressionType.subquery_t) {
					tables.addAll(analyzeSelectStatement(field.getExpr()
							.getRightOperand().getSubQuery()));
				}
			}
		}
		fullStatementDetails.put(queryDescription.getQuery(), queryDescription);
		if (delete.joins != null) {
			for (int i = 0; i < delete.joins.size(); i++) {
				TJoin join = delete.joins.getJoin(i);
				if (join.getTable().isBaseTable())
					tables.add(join.getTable().getFullName());
				TJoinItemList items = join.getJoinItems();
				if (items != null) {
					for (int j = 0; j < items.size(); j++) {
						TJoinItem item = items.getJoinItem(j);
						if (item.getTable().isBaseTable())
							tables.add(item.getTable().getFullName());
						if (item.getOnCondition() != null) {
							new tablesInExpr(this, item.getOnCondition(),
									tables).searchTable();
						}
					}
				}
			}
		}

		if (delete.getWhereClause() != null
				&& delete.getWhereClause().getCondition() != null) {

			new tablesInExpr(this, delete.getWhereClause().getCondition(),
                    tables).searchTable();
        }
        if (!tables.isEmpty()) {
            performLogging("Source: ");
            String[] tableArray = tables.toArray(new String[tables.size()]);
            for (int i = 0; i < tableArray.length; i++) {
                performLogging(tableArray[i]);
                if (i < tableArray.length - 1) {
                    performLogging(", ");
                }
            }
            performLogging();
        }
        return tables;
    }


    private Set<String> analyzeUpdateStatement(TUpdateSqlStatement update) {
        if (update.getTargetTable() != null) {
            performLogging("Target: "
                    + update.getTargetTable().getFullName());
        }
        Set<String> tables = new LinkedHashSet<>();
        tables.add(update.getTargetTable().getFullName());
        if (null == fullStatementDetails) {
            fullStatementDetails = new HashMap<>();
        }

        QueryBeans queryDescription = new QueryBeans();

		queryDescription.setQueryId(++queryId + "");
		queryDescription.setQuery(update.toString());
        queryDescription.setStatementType("UPDATE");
        List<ColumnDetailsBean> updateColumnName = new ArrayList<>();

        if (update.getResultColumnList() != null) {
			for (int i = 0; i < update.getResultColumnList().size(); i++) {
				TResultColumn field = update.getResultColumnList()
				.getResultColumn(i);
                ColumnDetailsBean columnDetail = new ColumnDetailsBean();
                if (null != field.getExpr()
                        && null != field.getExpr().getLeftOperand()) {
                    columnDetail.setColumnName(field.getExpr()
                            .getLeftOperand().toString());
                }
                if (null != field.getExpr().getRightOperand())
                    if (field.getExpr().getRightOperand()
                            .getExpressionType() == EExpressionType.subquery_t) {
                        tables.addAll(analyzeSelectStatement(field.getExpr()
                                .getRightOperand().getSubQuery()));
                    }
                updateColumnName.add(columnDetail);
            }
            queryDescription.setSelectColumnName(updateColumnName);
        }

        if (update.joins != null) {
			for (int i = 0; i < update.joins.size(); i++) {
				TJoin join = update.joins.getJoin(i);
				if (join.getTable().isBaseTable())
					tables.add(join.getTable().getFullName());
				TJoinItemList items = join.getJoinItems();
				if (items != null) {
					for (int j = 0; j < items.size(); j++) {
						TJoinItem item = items.getJoinItem(j);
						if (item.getTable().isBaseTable())
							tables.add(item.getTable().getFullName());
						if (item.getOnCondition() != null) {
							new tablesInExpr(this, item.getOnCondition(),
									tables).searchTable();
						}
					}
				}
			}
		}

		if (update.getWhereClause() != null
				&& update.getWhereClause().getCondition() != null) {
			new tablesInExpr(this, update.getWhereClause().getCondition(),
					tables).searchTable();
            WhereCondition w;
            w = new WhereCondition(update.getWhereClause().getCondition());
            w.printColumn();
            if (null != WhereCondition.getColumnDetailsList()) {
                List<ColumnDetailsBean> whereClauseColumn = WhereCondition
                        .getColumnDetailsList().stream().collect(Collectors.toList());
                queryDescription.setWhereClauseColumnList(whereClauseColumn);
            }

            if (null != WhereCondition.getColumnDetailsList()) {
                WhereCondition.getColumnDetailsList().clear();
            }
        }
        fullStatementDetails.put(queryDescription.getQuery(), queryDescription);
		if (!tables.isEmpty()) {
            performLogging("Source: ");
            String[] tableArray = tables.toArray(new String[tables.size()]);
            for (int i = 0; i < tableArray.length; i++) {
                performLogging(tableArray[i]);
                if (i < tableArray.length - 1) {
                    performLogging(", ");
                }
            }
            performLogging();
        }
        if (update.tables != null) {
			Map<String, String> aliasMapTEmp = null;
			for (int i = 0; i < update.tables.size(); i++) {
				TTable table = update.tables.getTable(i);
				String aliasNameTemp = null;
				if (table.isBaseTable()) {
					tables.add(table.getFullName());
					List<String> aliasListTemp = null;
					if (null != table.getAliasClause()) {
						aliasNameTemp = table.getAliasClause().getAliasName()
						.toString();
						if (null == aliasNameOfTables) {
                            aliasNameOfTables = new HashMap<>();
                        }
                        if (null != aliasNameOfTables.get(table.getFullName())) {
                            aliasListTemp = aliasNameOfTables.get(table
                                    .getFullName());
                            if (!aliasListTemp.contains(aliasNameTemp)) {
                                aliasListTemp.add(aliasNameTemp);
							}
						} else {
                            aliasListTemp = new ArrayList<>();
                            aliasListTemp.add(aliasNameTemp);
                        }
                    }
					if (null == aliasNameOfTables) {
                        aliasNameOfTables = new HashMap<>();
                    }
                    aliasNameOfTables.put(table.getFullName(),
                            aliasListTemp);
                    if (null == aliasMapTEmp) {
                        aliasMapTEmp = new HashMap<>();
                    }
                    aliasMapTEmp.put(table.getFullName(),
                            aliasNameTemp);
                } else {
					if (null != table.getAliasClause()) {
						if (null == aliasMapTEmp) {
                            aliasMapTEmp = new HashMap<>();
                        }
                        aliasMapTEmp.put(table.getSubquery().toString(), table
								.getAliasClause().toString());
					}
				}

			}
			if (null != aliasMapTEmp) {
				queryDescription.setAliasMap(aliasMapTEmp);
			}

		}
		fullStatementDetails.put(queryDescription.getQuery(), queryDescription);
		return tables;
    }


    private Set<String> analyzeInsertStatement(TInsertSqlStatement insert) {
        Set<String> targets = new LinkedHashSet<>();
        if (insert.getTargetTable() != null) {
            if (insert.getTargetTable().isBaseTable()
                    && !targets.contains(insert.getTargetTable()
                    .getFullName()))
                targets.add(insert.getTargetTable().getFullName());
            else if (insert.getTargetTable().getSubquery() != null) {
                targets.addAll(analyzeSelectStatement(insert
                        .getTargetTable().getSubquery()));
            }
        }
        if (null == fullStatementDetails) {
            fullStatementDetails = new HashMap<>();
        }

        QueryBeans queryDescription = new QueryBeans();

		queryDescription.setQueryId(++queryId + "");
		queryDescription.setQuery(insert.toString());
        queryDescription.setStatementType("INSERT");
        List<ColumnDetailsBean> insertColumnName = new ArrayList<>();
        if (insert.getColumnList() != null) {
            for (int i = 0; i < insert.getColumnList().size(); i++) {
				TParseTreeNode intoValue = insert.getColumnList().getElement(i);
				ColumnDetailsBean columnBeans = new ColumnDetailsBean();
				columnBeans.setColumnName(intoValue.toString());
				insertColumnName.add(columnBeans);
			}
            queryDescription.setSelectColumnName(insertColumnName);
        }
        fullStatementDetails.put(queryDescription.getQuery(), queryDescription);
		if (insert.getInsertConditions() != null) {
			for (int i = 0; i < insert.getInsertConditions().size(); i++) {
				TInsertCondition intoCondition = insert.getInsertConditions()
				.getElement(i);
				if (intoCondition.getInsertIntoValues() != null) {
					for (int j = 0; j < intoCondition.getInsertIntoValues()
					.size(); j++) {
						TInsertIntoValue intoValue = intoCondition
						.getInsertIntoValues().getElement(j);
						if (intoValue.getTable() != null
								&& intoValue.getTable().isBaseTable()
								&& !targets.contains(intoValue.getTable()
										.getFullName()))
							targets.add(intoValue.getTable().getFullName());
                    }
                }
            }
        }

        if (!targets.isEmpty()) {
            performLogging("Target: ");
            String[] tableArray = targets.toArray(new String[targets.size()]);
            for (int i = 0; i < tableArray.length; i++) {
                performLogging(tableArray[i]);
                if (i < tableArray.length - 1) {
                    performLogging(", ");
                }
            }
            performLogging();
        }

        Set<String> sources = new LinkedHashSet<>();

        if (insert.getValues() != null) {
			for (int i = 0; i < insert.getValues().size(); i++) {
				TMultiTarget multiTarget = insert.getValues().getMultiTarget(i);
				if (multiTarget.getSubQuery() != null) {
					sources.addAll(analyzeSelectStatement(multiTarget
							.getSubQuery()));
				}

				for (int j = 0; j < multiTarget.getColumnList().size(); j++) {
					TResultColumn field = multiTarget.getColumnList()
					.getResultColumn(j);
					if (field.getExpr().getExpressionType() == EExpressionType.subquery_t) {
						sources.addAll(analyzeSelectStatement(field.getExpr()
								.getSubQuery()));
					}
				}
			}
		}

		if (insert.getSubQuery() != null) {
			sources.addAll(analyzeSelectStatement(insert.getSubQuery()));
        }

        if (!sources.isEmpty()) {
            performLogging("Source: ");
            String[] tableArray = sources.toArray(new String[sources.size()]);
            for (int i = 0; i < tableArray.length; i++) {
                performLogging(tableArray[i]);
                if (i < tableArray.length - 1) {
                    performLogging(", ");
                }
            }
            performLogging();
        }
        return targets;
    }


    private Set<String> analyzeSelectStatement(TSelectSqlStatement stmt) {
        QueryBeans queryDescription;
        Set<String> tables = new LinkedHashSet<>();
        if (null != stmt && !ignoreQueryList.contains(stmt.toString())) {
            if (stmt.getSetOperator() != TSelectSqlStatement.setOperator_none) {
				tables.addAll(analyzeSelectStatement(stmt.getLeftStmt()));
				tables.addAll(analyzeSelectStatement(stmt.getRightStmt()));
			} else {
				if (null == fullStatementDetails) {
                    fullStatementDetails = new HashMap<>();
                }

                queryDescription = new QueryBeans();

				queryDescription.setQueryId(++queryId + "");
                queryDescription.setStatementType("SELECT");
                queryDescription.setQuery(stmt.toString());
                List<ColumnDetailsBean> selectColumnName = new ArrayList<>();
                for (int i = 0; i < stmt.getResultColumnList().size(); i++) {
                    ColumnDetailsBean columnDetail = new ColumnDetailsBean();
                    TResultColumn field = stmt.getResultColumnList()
                            .getResultColumn(i);
                    columnDetail.setColumnName(field.toString());
                    selectColumnName.add(columnDetail);
                    if (field.getExpr().getExpressionType() == EExpressionType.subquery_t) {
                        tables.addAll(analyzeSelectStatement(field.getExpr()
								.getSubQuery()));
					}
					if(null != field.getExpr() && field.getExpr().getExpressionType() == EExpressionType.case_t){

						if (null != field.getExpr().getCaseExpression()
								&& null != field.getExpr().getCaseExpression()
								.getWhenClauseItemList()
								&& field.getExpr().getCaseExpression()
								.getWhenClauseItemList().size() > 0
								&& null != field.getExpr().getCaseExpression()
								.getWhenClauseItemList()
								.getWhenClauseItem(0)
								&& null != field.getExpr().getCaseExpression()
								.getWhenClauseItemList()
								.getWhenClauseItem(0).getReturn_expr()
								&& null != field.getExpr().getCaseExpression()
								.getWhenClauseItemList()
								.getWhenClauseItem(0).getReturn_expr()
								.getSubQuery()) {

							analyzeStatement(field.getExpr()
									.getCaseExpression()
									.getWhenClauseItemList()
									.getWhenClauseItem(0).getReturn_expr()
									.getSubQuery());
						}

					}

				}
                queryDescription.setSelectColumnName(selectColumnName);

                if (stmt.getWhereClause() != null
						&& stmt.getWhereClause().getCondition() != null) {
					new tablesInExpr(this,
							stmt.getWhereClause().getCondition(), tables)
							.searchTable();
                    WhereCondition w;
                    w = new WhereCondition(stmt.getWhereClause().getCondition());
                    w.printColumn();
                    if (null != WhereCondition.getColumnDetailsList()) {
                        List<ColumnDetailsBean> whereClauseColumn = WhereCondition
                                .getColumnDetailsList().stream().collect(Collectors.toList());
                        queryDescription
                                .setWhereClauseColumnList(whereClauseColumn);
                    }
                    if (null != WhereCondition.getColumnDetailsList()) {
                        WhereCondition.getColumnDetailsList().clear();
                    }
                }
                if (null != stmt.getGroupByClause()
						&& null != stmt.getGroupByClause().getHavingClause()) {
					new tablesInExpr(this, stmt.getGroupByClause()
							.getHavingClause(), tables).searchTable();
                    WhereCondition w;
                    w = new WhereCondition(stmt.getGroupByClause()
                            .getHavingClause());
                    w.printColumn();
                    if (null != WhereCondition.getColumnDetailsList()) {
                        List<ColumnDetailsBean> whereClauseColumn = WhereCondition
                                .getColumnDetailsList().stream().collect(Collectors.toList());
                        queryDescription
                                .setWhereClauseColumnList(whereClauseColumn);
                    }
                    if (null != WhereCondition.getColumnDetailsList()) {
                        WhereCondition.getColumnDetailsList().clear();
                    }
                }
                if (null == stmt.getWhereClause()
						&& null == stmt.getGroupByClause()
						&& null != stmt.joins) {
					for (int i = 0; i < stmt.joins.size(); i++) {
						TJoin join = stmt.joins.getJoin(i);
						TJoinItemList joinList = join.getJoinItems();
						for (int j = 0; j < joinList.size(); j++) {
							new tablesInExpr(this, joinList.getJoinItem(i)
									.getOnCondition(), tables).searchTable();
                            WhereCondition w;
                            w = new WhereCondition(joinList.getJoinItem(i)
                                    .getOnCondition());
                            w.printColumn();
                            if (null != WhereCondition.getColumnDetailsList()) {
                                List<ColumnDetailsBean> whereClauseColumn = WhereCondition
                                        .getColumnDetailsList().stream().collect(Collectors.toList());
                                queryDescription
                                        .setWhereClauseColumnList(whereClauseColumn);
                            }
                            if (null != WhereCondition.getColumnDetailsList()) {
                                WhereCondition.getColumnDetailsList().clear();
                            }
                        }
					}
				}
				fullStatementDetails.put(queryDescription.getQuery(),
						queryDescription);
				if (stmt.joins != null) {
					for (int i = 0; i < stmt.joins.size(); i++) {
						TJoin join = stmt.joins.getJoin(i);
						if (join.getTable().isBaseTable())
							tables.add(join.getTable().getFullName());
						else if (join.getTable().getSubquery() != null) {
							tables.addAll(analyzeSelectStatement(join
									.getTable().getSubquery()));
						}
						TJoinItemList items = join.getJoinItems();
						if (items != null) {
							for (int j = 0; j < items.size(); j++) {
								TJoinItem item = items.getJoinItem(j);
								if (item.getTable().isBaseTable())
									tables.add(item.getTable().getFullName());
								else if (item.getTable().getSubquery() != null) {
									tables.addAll(analyzeSelectStatement(item
											.getTable().getSubquery()));
								}
								if (item.getOnCondition() != null) {
									new tablesInExpr(this,
											item.getOnCondition(), tables)
											.searchTable();
								}
							}
						}
					}
				}


				if (stmt.getTargetTable() != null) {
					if (stmt.getTargetTable().isBaseTable())
						tables.add(stmt.getTargetTable().getFullName());
					else if (stmt.getTargetTable().getSubquery() != null) {
						tables.addAll(analyzeSelectStatement(stmt
								.getTargetTable().getSubquery()));
					}
				}


				if (stmt.tables != null) {
					Map<String, String> aliasMapTEmp = null;
					for (int i = 0; i < stmt.tables.size(); i++) {
						TTable table = stmt.tables.getTable(i);
						String aliasNameTemp = null;
						if (table.isBaseTable()) {
							tables.add(table.getFullName());
							List<String> aliasListTemp = null;
							if (null != table.getAliasClause()) {
								aliasNameTemp = table.getAliasClause()
										.getAliasName().toString();
								if (null == aliasNameOfTables) {
                                    aliasNameOfTables = new HashMap<>();
                                }
                                if (null != aliasNameOfTables.get(table
                                        .getFullName())) {
                                    aliasListTemp = aliasNameOfTables.get(table
                                            .getFullName());
                                    if (!aliasListTemp.contains(aliasNameTemp)) {
                                        aliasListTemp.add(aliasNameTemp);
									}
								} else {
                                    aliasListTemp = new ArrayList<>();
                                    aliasListTemp.add(aliasNameTemp);
                                }
                            }
							if (null == aliasNameOfTables) {
                                aliasNameOfTables = new HashMap<>();
                            }
                            aliasNameOfTables.put(table.getFullName(), aliasListTemp);
                            if (null == aliasMapTEmp) {
                                aliasMapTEmp = new HashMap<>();
                            }
                            aliasMapTEmp.put(table.getFullName(),
                                    aliasNameTemp);
                        } else {
							if (null != table.getAliasClause()) {
								if (null == aliasMapTEmp) {
                                    aliasMapTEmp = new HashMap<>();
                                }
                                aliasMapTEmp.put(
										table.getSubquery().toString(), table
												.getAliasClause().toString());
							}
						}

					}
					if (null != aliasMapTEmp) {
						queryDescription.setAliasMap(aliasMapTEmp);
					}

				}
				fullStatementDetails.put(queryDescription.getQuery(),
                        queryDescription);
            }
        }
        return tables;
    }

    class tablesInExpr implements IExpressionVisitor {

        private Set<String> tables;
        private TExpression expr;
        private SQLParserUtil impact;

        public tablesInExpr(SQLParserUtil impact, TExpression expr,
                            Set<String> tables) {
            this.impact = impact;
            this.expr = expr;
            this.tables = tables;
        }

        private void addColumnToList(TParseTreeNodeList list) {
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    List<TExpression> exprList = new ArrayList<>();
                    Object element = list.getElement(i);

                    if (element instanceof TGroupByItem) {
                        exprList.add(((TGroupByItem) element).getExpr());
                    }
                    if (element instanceof TOrderByItem) {
                        exprList.add(((TOrderByItem) element).getSortKey());
                    } else if (element instanceof TExpression) {
                        exprList.add((TExpression) element);
                    } else if (element instanceof TWhenClauseItem) {
                        exprList.add(((TWhenClauseItem) element)
                                .getComparison_expr());
                        exprList.add(((TWhenClauseItem) element)
                                .getReturn_expr());
                    }

                    for (TExpression expr : exprList) {
                        expr.inOrderTraverse(this);
                    }
                }
            }
        }

        public boolean exprVisit(TParseTreeNode pNode, boolean isLeafNode) {
            TExpression lexer = (TExpression) pNode;
            if (lexer.getExpressionType() == EExpressionType.simple_object_name_t) {

            } else if (lexer.getExpressionType() == EExpressionType.between_t) {

            } else if (lexer.getExpressionType() == EExpressionType.function_t) {
                TFunctionCall func = lexer.getFunctionCall();
                if (func.getFunctionType() == EFunctionType.trim_t) {
                    TTrimArgument args = func.getTrimArgument();
                    TExpression expr = args.getStringExpression();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                    expr = args.getTrimCharacter();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                } else if (func.getFunctionType() == EFunctionType.cast_t) {
                    TExpression expr = func.getExpr1();
                    if (expr != null
                            && !expr.toString().trim().equals("*")
                            || func.getFunctionType() == EFunctionType.extract_t) {
                        assert expr != null;
                        expr.inOrderTraverse(this);
                    }
                } else if (func.getFunctionType() == EFunctionType.convert_t) {
                    TExpression expr = func.getExpr1();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                    expr = func.getExpr2();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                } else if (func.getFunctionType() == EFunctionType.contains_t
                        || func.getFunctionType() == EFunctionType.freetext_t) {
                    TExpression expr = func.getExpr1();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                    TInExpr inExpr = func.getInExpr();
                    if (inExpr.getExprList() != null) {
                        for (int k = 0; k < inExpr.getExprList().size(); k++) {
                            expr = inExpr.getExprList().getExpression(k);
                            if (expr.toString().trim().equals("*"))
                                continue;
                            expr.inOrderTraverse(this);
                        }
                        if (expr != null && !expr.toString().trim().equals("*")) {
                            expr.inOrderTraverse(this);
                        }
                    }
                    expr = inExpr.getFunc_expr();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                } else if (func.getFunctionType() == EFunctionType.extractxml_t) {
                    TExpression expr = func.getXMLType_Instance();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                    expr = func.getXPath_String();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                    expr = func.getNamespace_String();
                    if (expr != null && !expr.toString().trim().equals("*")) {
                        expr.inOrderTraverse(this);
                    }
                }

                if (func.getFunctionType() == EFunctionType.rank_t) {
                    TOrderByItemList orderByList = func.getOrderByList();
                    for (int k = 0; k < orderByList.size(); k++) {
                        TExpression expr = orderByList.getOrderByItem(k)
                                .getSortKey();
                        if (expr.toString().trim().equals("*"))
                            continue;
                        expr.inOrderTraverse(this);
                    }
                } else if (func.getArgs() != null) {
                    for (int k = 0; k < func.getArgs().size(); k++) {
                        TExpression expr = func.getArgs().getExpression(k);
                        if (expr.toString().trim().equals("*"))
                            continue;
                        expr.inOrderTraverse(this);
                    }
                }
                if (func.getAnalyticFunction() != null) {
                    TParseTreeNodeList list = func.getAnalyticFunction()
                            .getPartitionBy_ExprList();
                    addColumnToList(list);

                    if (func.getAnalyticFunction().getOrderBy() != null) {
                        list = func.getAnalyticFunction().getOrderBy()
                                .getItems();
                        addColumnToList(list);
                    }
                }

            } else if (lexer.getExpressionType() == EExpressionType.subquery_t) {
                tables.addAll(impact.analyzeSelectStatement(lexer
                        .getSubQuery()));
            } else if (lexer.getExpressionType() == EExpressionType.case_t) {
                TCaseExpression expr = lexer.getCaseExpression();
                TExpression conditionExpr = expr.getInput_expr();
                if (conditionExpr != null) {
                    conditionExpr.inOrderTraverse(this);
                }
                TExpression defaultExpr = expr.getElse_expr();
                if (defaultExpr != null) {
                    defaultExpr.inOrderTraverse(this);
                }
                TWhenClauseItemList list = expr.getWhenClauseItemList();
                addColumnToList(list);
            }
            return true;
        }

        public void searchTable() {
            this.expr.inOrderTraverse(this);
        }
    }
}