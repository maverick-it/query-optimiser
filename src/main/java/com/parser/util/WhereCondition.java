package com.parser.util;

import com.bean.ColumnDetailsBean;
import gudusoft.gsqlparser.EExpressionType;
import gudusoft.gsqlparser.nodes.IExpressionVisitor;
import gudusoft.gsqlparser.nodes.TExpression;
import gudusoft.gsqlparser.nodes.TParseTreeNode;

import java.util.ArrayList;
import java.util.List;

import com.util.LoggerUtil;

public class WhereCondition implements IExpressionVisitor {

    private static List<ColumnDetailsBean> columnDetailsList;
    private TExpression condition;

	public WhereCondition(TExpression expr) {
		this.condition = expr;
	}

    public static List<ColumnDetailsBean> getColumnDetailsList() {
        return columnDetailsList;
    }

	public void printColumn() {
		this.condition.inOrderTraverse(this);
	}

	boolean is_compare_condition(EExpressionType t) {
		return ((t == EExpressionType.simple_comparison_t)
				|| (t == EExpressionType.group_comparison_t)
				|| (t == EExpressionType.in_t) || (t == EExpressionType.null_t));
	}

	public boolean exprVisit(TParseTreeNode pnode, boolean pIsLeafNode) {
		TExpression lcexpr = (TExpression) pnode;
		if (is_compare_condition(lcexpr.getExpressionType())) {
            TExpression leftExpr = lcexpr.getLeftOperand();

			ColumnDetailsBean columnDetail = new ColumnDetailsBean();

			if (lcexpr.getComparisonOperator() != null) {

				columnDetail.setOperator(lcexpr.getComparisonOperator().astext);
			} else if (null != lcexpr.getNotToken()
					&& null != lcexpr.getOperatorToken()
					&& ("IN".equalsIgnoreCase(lcexpr.getOperatorToken().astext) || "NULL"
							.equalsIgnoreCase(lcexpr.getOperatorToken().astext))) {

				columnDetail.setOperator(lcexpr.getNotToken() + " "
						+ lcexpr.getOperatorToken().astext);

			} else if (null != lcexpr.getOperatorToken()
					&& ("IN".equalsIgnoreCase(lcexpr.getOperatorToken().astext))) {
				columnDetail.setOperator(lcexpr.getOperatorToken().astext);
			}

			columnDetail.setColumnName(leftExpr.toString());
			columnDetail.setColumnValue(null == lcexpr.getRightOperand() ? ""
					: lcexpr.getRightOperand().toString());
			LoggerUtil.performLogging("");


            if (null == columnDetailsList) {
                columnDetailsList = new ArrayList<>();
            }

            columnDetailsList.add(columnDetail);

		}
		return true;
	}

}