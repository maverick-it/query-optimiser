SQL Optimizer
-------------

This is a sql optimizer core Engine.(Runs on Java 8)

SQL Optimise runs on certain generic SQL Tunning rules and suggests optimisation based upon the query explain plan.

## How to Set Up:
1) This is a maven project and runs on Java8. So these are the pre requisite.

2) Execute below command being in project main directory:

mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.3.0 -Dpackaging=jar -Dfile=lib/ojdbc14-10.2.0.3.0.jar -DgeneratePom=true

mvn install:install-file -DgroupId=com.gsp -DartifactId=gsp -Dversion=1.0 -Dpackaging=jar -Dfile=lib/gsp.jar -DgeneratePom=true
